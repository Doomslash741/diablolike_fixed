/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.identifying;

import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class Identifier {
	private ItemStack item;
	private Range level;
	private static final Pattern PATTERN = Pattern.compile(
			MainConfig.LEVEL_RANGE_PATTERN.replaceAll("<lvl_min>", "[0-9]+").replaceAll("<lvl_max>", "[0-9]+")),
			ULTRA_PATTERN = Pattern.compile("Identifikuje předměty jakéhokoliv levelu.");

	private Identifier(ItemStack item, Range level) {
		this.item = item;
		this.level = level;
	}

	@Nullable
	public static Identifier getIdentifier(ItemStack item) {
		if (!Identifier.isIdentifier(item)) {
			return null;
		}
		String rangeLore = Identifier.identifierRangeFromLore(item.getItemMeta().getLore());
		int lvlMin = 0;
		int lvlMax = Integer.MAX_VALUE;
		String[] patternSplit = MainConfig.LEVEL_RANGE_PATTERN.split(" ");
		String[] split = rangeLore.split(" ");
		for (int i = 0; i < split.length; i++) {
			String s = split[i];
			if (s.isEmpty()) {
				continue;
			}
			s = ChatColor.stripColor(s);
			if (patternSplit[i].equalsIgnoreCase("<lvl_min>")) {
				try {
					lvlMin = Integer.parseInt(s);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			if (patternSplit[i].equalsIgnoreCase("<lvl_max>."))
				try {
					lvlMax = Integer.parseInt(s.replaceAll("[.]", ""));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
		}
		return new Identifier(item, new Range(lvlMin, lvlMax));
	}

	public static boolean isIdentifier(ItemStack item) {
		return new ItemUtils(item).isValid(true, true)
				&& MainConfig.DISPLAY_NAME_PATTERN
						.matcher(ChatColor.stripColor((String) item.getItemMeta().getDisplayName())).find()
				&& !Identifier.identifierRangeFromLore(item.getItemMeta().getLore()).isEmpty();
	}

	private static String identifierRangeFromLore(List<String> lore) {
		for (String s : lore) {
			if (s.isEmpty())
				continue;
			String strip = ChatColor.stripColor((String) s);
			if (PATTERN.matcher(strip).find() || ULTRA_PATTERN.matcher(strip).find()) {
				return strip;
			}

		}
		return "";
	}

	public Identify toIdentify(ItemStack item) {
		return new Identify(item);
	}

	public boolean canIdentify(ItemStack item) {
		return level.isInRange(Utils.fromDisplayName(item).get(0).getLevel())
				&& !toIdentify(item).isIdentified();
	}

	public ItemStack getItem() {
		return this.item;
	}
}
