/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.rit.sucy.CustomEnchantment
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 */
package git.doomshade.diablolike.identifying;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.rit.sucy.CustomEnchantment;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.TierColor;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.drops.sell.Attribute;
import git.doomshade.diablolike.events.IdentifyEvent;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class Identify {
	public static final String ODDELENI = "\u00a78----------------------";
	public static final String GEM = "\u00a72(M�sto pro klenot)";
	private ItemStack item;
	private ItemUtils utils;
	private DiabloItem dItem = null;
	private static ConsoleCommandSender sender = Bukkit.getConsoleSender();;

	public DiabloItem getDiabloItem() {
		return this.dItem;
	}

	public Identify(ItemStack item) {
		this.item = item;
		utils = new ItemUtils(item);
	}

	public Identify(String item) {
		this(ItemUtils.getItemFromFile(item));
	}

	public boolean isIdentified() {
		if (item == null || !item.hasItemMeta()) {
			return true;
		}
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasLore()) {
			return true;
		}
		for (String s : meta.getLore()) {
			if (s.isEmpty()) {
				continue;
			}
			if (Pattern.compile("[0-9]+-[0-9]+").matcher(s).find()) {
				return false;
			}
		}
		return !this.item.getItemMeta().getLore().contains(MainConfig.UNIDENTIFIED_ITEM_LORE.get().toString());
	}

	

	public void reidentify() {
		setIdentified(false);
		setIdentified(true);
	}

	private List<DiabloItem> getPossibleItems() {
		ArrayList<DiabloItem> possibleItems = new ArrayList<DiabloItem>(Utils.fromDisplayName(this.item));
		if (possibleItems.isEmpty()) {
			sender.sendMessage("1 " + item.getItemMeta().getDisplayName());
			sender.sendMessage("no items found");
			return new ArrayList<>();
		}
		if (!utils.isIdentifiable()) {
			sender.sendMessage("nonidentifiable");
			return new ArrayList<>();
		}

		ArrayList<DiabloItem> items = new ArrayList<DiabloItem>(possibleItems);
		for (DiabloItem item : items) {
			if (item.getItem().getType() == this.item.getType())
				continue;
			possibleItems.remove(item);
		}
		return possibleItems;
	}

	private boolean includeGem() {
		for (String dName : MainConfig.getExcludedGemDisplayNames()) {
			if (ChatColor.stripColor(item.getItemMeta().getDisplayName().toLowerCase()).contains(dName)) {
				return false;
			}
		}
		double randomChance = new Random().nextDouble() * 100.0;
		return randomChance < Utils.GEM_CHANCES.get(utils.getTierColor())
				&& !MainConfig.getExcludedGemMaterials().contains(item.getType());
	}

	public void setIdentified(boolean identify) {
		if (!utils.isIdentifiable()) {
			return;
		}
		int i;
		ItemMeta meta = item.getItemMeta();
		ArrayList<String> additionalLore = new ArrayList<String>();
		if (!identify) {
			additionalLore.add(MainConfig.UNIDENTIFIED_ITEM_LORE.get().toString());
			additionalLore.add("");
			additionalLore.addAll(utils.getRestOfLore());
			meta.setLore(additionalLore);
			item.setItemMeta(meta);
			updateItemStats();
			return;
		}
		TierColor tierColor = utils.getTierColor();
		List<DiabloItem> possibleItems = new ArrayList<DiabloItem>(getPossibleItems());
		if (possibleItems.isEmpty()) {
			return;
		}

		List<String> finalLore = new ArrayList<String>();

		if (includeGem()) {
			additionalLore.add(ChatColor.DARK_GREEN + "(M�sto pro klenot)");
		}
		additionalLore.addAll(utils.getRestOfLore());

		DiabloItem dItem = possibleItems.get(new Random().nextInt(possibleItems.size()));
		ItemStack before = dItem.getItem();
		if (dItem.getEnchantments() != null || dItem.getCustomEnchantments() != null) {
			finalLore.add("");
		}

		List<String> cfgLore = dItem.getLore();
		List<Integer> attributeIndex = new ArrayList<Integer>();
		List<String> nonAttributes = new ArrayList<String>();
		for (int i2 = 0; i2 < cfgLore.size(); ++i2) {
			String loreLine = ChatColor.translateAlternateColorCodes('&', cfgLore.get(i2));
			if (loreLine.equalsIgnoreCase(GEM) || loreLine.equalsIgnoreCase(ODDELENI))
				break;
			if (!isAttribute(loreLine)) {
				nonAttributes.add(cfgLore.get(i2));
			} else {
				attributeIndex.add(i2);
			}
		}
		double attrAmount = Math.ceil((double) attributeIndex.size() / Utils.CONFIG_ATTRIBUTE_AMOUNT.get(tierColor));

		List<Integer> selectedAttributesIndex = new ArrayList<Integer>();
		List<Integer> reqAttrIndex = getRequiredAttributePosition(cfgLore);
		if (!reqAttrIndex.isEmpty()) {
			for (i = 0; i < reqAttrIndex.size() && attrAmount != 0.0; attrAmount -= 1.0, ++i) {
				selectedAttributesIndex.add(i);
			}
		}
		i = 0;
		Random random = new Random();
		while (i < attrAmount) {
			int num = attributeIndex.get(random.nextInt(attributeIndex.size()));
			while (!selectedAttributesIndex.contains(num)) {
				selectedAttributesIndex.add(num);
				++i;
			}
		}
		selectedAttributesIndex.sort(null);

		for (int num : selectedAttributesIndex) {
			finalLore.add(ChatColor.translateAlternateColorCodes('&', cfgLore.get(num)));
		}
		finalLore.addAll(nonAttributes);
		finalLore.addAll(additionalLore);
		meta.setLore(finalLore);
		this.item.setItemMeta(meta);
		if (dItem.getEnchantments() != null) {
			this.item.addUnsafeEnchantments(dItem.getEnchantments());
		}
		if (dItem.getCustomEnchantments() != null) {
			for (CustomEnchantment ench : dItem.getCustomEnchantments().keySet()) {
				ench.addToItem(this.item, dItem.getCustomEnchantments().get(ench).getRandom());
			}
		}
		this.updateItemStats();
		this.dItem = dItem;
		ItemStack after = dItem.getItem();
		Bukkit.getPluginManager().callEvent(new IdentifyEvent(before, after, identify));
	}

	private void updateItemStats() {
		ItemMeta meta = this.item.getItemMeta();
		meta.setLore(this.updatedLore());
		this.item.setItemMeta(meta);
		new BukkitRunnable() {

			@Override
			public void run() {
				new ItemUtils(item).updatePrice();
			}
		}.runTaskLaterAsynchronously((Plugin) DiabloLike.getInstance(), 1L);
	}

	@SuppressWarnings("unused")
	private List<Integer> getAdditionalAttributes(List<String> lore) {
		ArrayList<Integer> positions = new ArrayList<Integer>();
		for (int i = 0; i < lore.size(); ++i) {
			if (lore.get(i).isEmpty() || lore.get(i) == "")
				continue;
			for (Pattern pattern : Utils.STAFF_LORE) {
				Matcher m = pattern.matcher(lore.get(i));
				if (!m.find())
					continue;
				positions.add(i);
			}
		}
		return positions;
	}

	private List<Integer> getRequiredAttributePosition(List<String> lore) {
		int i;
		Matcher matcher;
		ArrayList<Integer> positions = new ArrayList<Integer>();
		Pattern pattern = Pattern.compile("Po�kozen�");
		for (i = 0; i < lore.size(); ++i) {
			matcher = pattern.matcher(lore.get(i));
			if (!matcher.find())
				continue;
			positions.add(i);
			break;
		}
		pattern = Pattern.compile("Obrana");
		for (i = 0; i < lore.size(); ++i) {
			matcher = pattern.matcher(lore.get(i));
			if (!matcher.find())
				continue;
			positions.add(i);
			break;
		}
		return positions;
	}

	public static boolean isAttribute(String s) {
		return Attribute.DefaultAttributes.NOCOLOR_NAME_MAP
				.containsKey(ChatColor.stripColor(s.replaceAll("[+]\\d+-\\d+.? ", "").replaceAll(": \\d+-\\d+", "")
						.replaceAll("[+]\\d+.? ", "").replaceAll(": \\d+", "")));
	}

	private List<String> updatedLore() {
		ArrayList<String> lore = new ArrayList<String>();
		for (String s : this.item.getItemMeta().getLore()) {
			lore.add(Identify.replaceStringWithRandom(s));
		}
		return lore;
	}

	public static String replaceStringWithRandom(String s) {
		String rangeOnly = "[^\\d-\u00a7]+";
		String strNumRange = s.replaceAll(rangeOnly, "");
		String regex = "\u00a7?[0-9]+[-][0-9]+";
		strNumRange = ChatColor.stripColor((String) s).replaceAll("[^\\d-]", "");
		if (strNumRange.matches(regex)) {
			String[] split = strNumRange.split("-");
			String rn = String.valueOf(new Range(Integer.parseInt(split[0]), Integer.parseInt(split[1])).getRandom());
			return s.replaceAll(regex, rn);
		}
		return s;
	}

}
