package git.doomshade.diablolike.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class IdentifyEvent extends Event {
	private static HandlerList handlerList = new HandlerList();
	private ItemStack before, after;
	private boolean identified;
	
	public IdentifyEvent(ItemStack before, ItemStack after, boolean identified) {
		// TODO Auto-generated constructor stub
		this.before = before;
		this.after = after;
		this.identified = identified;
	}

	@Override
	public HandlerList getHandlers() {
		// TODO Auto-generated method stub
		return handlerList;
	}
	
	public static HandlerList getHandlerList() {
		return handlerList;
	}
	
	public ItemStack getBeforeIdentify() {
		return before;
	}
	
	public ItemStack getAfterIdentify() {
		return after;
	}
	
	public boolean isIdentified() {
		return identified;
	}

}
