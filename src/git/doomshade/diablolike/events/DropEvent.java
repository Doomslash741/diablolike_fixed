package git.doomshade.diablolike.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class DropEvent extends Event {
	private static HandlerList handlerList = new HandlerList();
	private ItemStack drop;

	public DropEvent(ItemStack drop) {
		// TODO Auto-generated constructor stub
		this.drop = drop;
	}

	@Override
	public HandlerList getHandlers() {
		// TODO Auto-generated method stub
		return handlerList;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}
	
	public ItemStack getDrop() {
		return drop;
	}
}
