/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.event.Event
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.inventory.InventoryClickEvent
 */
package git.doomshade.diablolike.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;

public class GUIClickEvent
extends Event {
    private static HandlerList handlers = new HandlerList();
    private InventoryClickEvent e;

    @Override
	public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public GUIClickEvent(InventoryClickEvent e) {
        this.e = e;
    }

    public InventoryClickEvent getEvent() {
        return this.e;
    }
}

