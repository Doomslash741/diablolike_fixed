/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  net.citizensnpcs.api.event.NPCRightClickEvent
 *  net.citizensnpcs.api.npc.NPC
 *  net.citizensnpcs.api.trait.Trait
 *  net.citizensnpcs.api.trait.TraitName
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.Server
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.vendors;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.utils.ItemUtils;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.trait.TraitName;

@TraitName(value = "diablovendor")
public class VendorTrait extends Trait {
	public VendorTrait() {
		super("diablovendor");
	}

	@Override
	public void onAttach() {
		DiabloLike.getInstance().getServer().getLogger().info(this.npc.getName() + " has been assigned DiabloVendor!");
	}

	@EventHandler
	public void onRightClick(NPCRightClickEvent e) {
		if (e.getNPC() != this.getNPC()) {
			return;
		}
		e.getClicker().openInventory(VendorTrait.getMenu());
	}

	public static Inventory getMenu() {
		Inventory inv = Bukkit.createInventory(null, (int) 54, (String) MainConfig.VENDOR_INVENTORY_NAME);
		for (int i = 0; i < inv.getSize(); ++i) {
			if (VendorTrait.isInRange(i)) {
				inv.setItem(i, VendorTrait.glass());
				continue;
			}
			if (i % 9 != 4)
				continue;
			inv.setItem(i, VendorTrait.rails());
		}
		inv.setItem(MainConfig.VENDOR_MONEY_POSITION, VendorTrait.moneyInfoItem(0.0));
		inv.setItem(MainConfig.VENDOR_ACCEPT_BUTTION_POSITION, MainConfig.getVendorAcceptButtonItem());
		inv.setItem(MainConfig.VENDOR_DECLINE_BUTTON_POSITION, MainConfig.getVendorDeclineButtonItem());
		inv.setItem(MainConfig.VENDOR_INFO_POSITION, MainConfig.getVendorInfoItem());
		return inv;
	}

	private static final ItemStack rails() {
		return ItemUtils.makeItem(Material.ACTIVATOR_RAIL, "\u00a7a", new ArrayList<String>(), (short) 0);
	}

	private static boolean isInRange(int a) {
		for (int i = 0; i <= 54; i += 9) {
			for (int s = 0; s <= 8; ++s) {
				int slot = s + i;
				if (s < 5 || a != slot)
					continue;
				return true;
			}
		}
		return false;
	}

	private static final ItemStack glass() {
		return ItemUtils.makeItem(Material.STAINED_GLASS_PANE, "\u00a7a", new ArrayList<String>(), (short) 15);
	}

	public static ItemStack moneyInfoItem(double money) {
		ItemStack item = MainConfig.getVendorMoneyItem().clone();
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.getLore();
		for (int i = 0; i < lore.size(); ++i) {
			lore.set(i, ChatColor.translateAlternateColorCodes((char) '&',
					(String) ((String) lore.get(i)).replaceAll("%money", String.valueOf(money))));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
}
