/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.rit.sucy.CustomEnchantment
 *  com.rit.sucy.EnchantmentAPI
 *  net.citizensnpcs.api.CitizensAPI
 *  net.citizensnpcs.api.trait.TraitInfo
 *  net.milkbowl.vault.chat.Chat
 *  net.milkbowl.vault.economy.Economy
 *  net.milkbowl.vault.permission.Permission
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Server
 *  org.bukkit.configuration.file.FileConfigurationOptions
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.event.Listener
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 *  org.bukkit.plugin.RegisteredServiceProvider
 *  org.bukkit.plugin.ServicesManager
 *  org.bukkit.plugin.java.JavaPlugin
 */
package git.doomshade.diablolike;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.rit.sucy.CustomEnchantment;
import com.rit.sucy.EnchantmentAPI;

import git.doomshade.diablolike.cmds.CommandHandler;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.crafting.Crafting;
import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.enchantments.Rychlostrelba;
import git.doomshade.diablolike.gui.GUIMain;
import git.doomshade.diablolike.listeners.AttributeListener;
import git.doomshade.diablolike.listeners.GUIListener;
import git.doomshade.diablolike.listeners.MobLevel;
import git.doomshade.diablolike.listeners.NPCListener;
import git.doomshade.diablolike.listeners.OrbListener;
import git.doomshade.diablolike.listeners.PlayerActions;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.Initializer;
import git.doomshade.diablolike.utils.Utils;
import git.doomshade.diablolike.vendors.VendorTrait;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

public class DiabloLike
extends JavaPlugin {
    private static DiabloLike instance;
    private File COLLECTIONS_FOLDER;
    private File DUNGEONS_FOLDER;
    private File SPECIFIC_LOOT_FOLDER;
    private File[] dungeonFiles;
    private File[] mythicMobFiles;
    private Economy econ;
    private Permission perms;
    private Chat chat;
    private File messagesFile;
    private File itemsFile;
    private File orbsFile;

    public static DiabloLike getInstance() {
        return instance;
    }

    private static void setInstance(DiabloLike instance) {
        DiabloLike.instance = instance;
    }

    public List<File> getAllItemFiles() {
        ArrayList<File> files = new ArrayList<File>();
        files.addAll(Arrays.asList(this.getCOLLECTIONS_FOLDER().listFiles()));
        files.addAll(Arrays.asList(this.getSpecificLootFolder().listFiles()));
        files.add(this.getItems());
        return files;
    }

    public File getCOLLECTIONS_FOLDER() {
        return this.COLLECTIONS_FOLDER;
    }

    public Crafting getCrafting() {
        return Crafting.getInstance();
    }

    public File[] getDungeonFiles() {
        return this.dungeonFiles;
    }

    public File getDUNGEONS_FOLDER() {
        return this.DUNGEONS_FOLDER;
    }

    public Economy getEconomy() {
        return this.econ;
    }

    public Chat getChat() {
        return this.chat;
    }

    public File getItems() {
        return this.itemsFile;
    }

    public File getMessages() {
        return this.messagesFile;
    }

    public File[] getMythicMobFiles() {
        return this.mythicMobFiles;
    }

    public File getOrbsFile() {
        return this.orbsFile;
    }

    public Permission getPermissions() {
        return this.perms;
    }

    public String getPluginName() {
        return String.format("[%s] ", this.getName());
    }

    public File getSpecificLootFolder() {
        return this.SPECIFIC_LOOT_FOLDER;
    }

    public void loadFiles() {
        this.sendConsoleMessage("Loading files...");
        this.messagesFile = new File(this.getDataFolder() + File.separator + "messages.yml");
        this.itemsFile = new File(this.getDataFolder() + File.separator + "items.yml");
        this.orbsFile = new File(this.getDataFolder(), "orbs.yml");
        this.COLLECTIONS_FOLDER = new File(this.getDataFolder() + File.separator + "collections");
        this.DUNGEONS_FOLDER = new File(this.getDataFolder() + File.separator + "dungeons");
        this.SPECIFIC_LOOT_FOLDER = new File(this.getDataFolder() + File.separator + "mobspecific");
    }

    @Override
	public void onEnable() {
        DiabloLike.setInstance(this);
        this.loadFiles();
        this.makeDirectories();
        this.registerEnchantments();
        try {
            this.setupFiles();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        this.registerEvents();
        this.registerTraits();
        this.setupCmds();
        GUIMain.init();
    }

    public void setItemsFile(File items) {
        this.itemsFile = items;
    }

    public void setMessagesFile(File messages) {
        this.messagesFile = messages;
    }

    public void setupFiles() throws IOException {
        this.sendConsoleMessage("Setting up files...");
        if (!this.getDataFolder().isDirectory()) {
            this.getDataFolder().mkdir();
        }
        this.dungeonFiles = this.DUNGEONS_FOLDER.listFiles();
        if (!this.getMessages().exists()) {
            this.getMessages().createNewFile();
        }
        if (!this.itemsFile.exists()) {
            this.itemsFile.createNewFile();
        }
        if (!this.orbsFile.exists()) {
            this.orbsFile.createNewFile();
        }
        YamlConfiguration messagesLoader = YamlConfiguration.loadConfiguration((File)this.messagesFile);
        messagesLoader.addDefaults(Messages.defaultMessages());
        messagesLoader.options().copyDefaults(true);
        messagesLoader.save(this.messagesFile);
        Messages.writeMessages();
        MainConfig.loadFiles();
        this.saveDefaultConfig();
        this.setMythicMobFiles(new File("plugins/MythicMobs/Mobs").listFiles(new Dungeon("")));
        Initializer.initFiles();
    }

    private void makeDirectories() {
        this.sendConsoleMessage("Creating directories...");
        if (!this.COLLECTIONS_FOLDER.isDirectory()) {
            this.COLLECTIONS_FOLDER.mkdirs();
        }
        if (!this.DUNGEONS_FOLDER.isDirectory()) {
            this.DUNGEONS_FOLDER.mkdirs();
        }
        if (!this.SPECIFIC_LOOT_FOLDER.isDirectory()) {
            this.SPECIFIC_LOOT_FOLDER.mkdirs();
        }
    }

    private void registerEnchantments() {
        if (Bukkit.getPluginManager().getPlugin("EnchantmentAPI").isEnabled()) {
            this.sendConsoleMessage("Registering custom enchantments...");
            EnchantmentAPI.registerCustomEnchantment((CustomEnchantment)new Rychlostrelba());
        }
    }

    private void registerEvents() {
        this.sendConsoleMessage("Registering events...");
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents((Listener)new MobLevel(), (Plugin)this);
        pm.registerEvents((Listener)new PlayerActions(), (Plugin)this);
        pm.registerEvents((Listener)new OrbListener(), (Plugin)this);
        pm.registerEvents((Listener)new AttributeListener(), (Plugin)this);
        pm.registerEvents((Listener)new NPCListener(), (Plugin)this);
        pm.registerEvents((Listener)this.getCrafting(), (Plugin)this);
        pm.registerEvents((Listener)new GUIListener(), (Plugin)this);
    }

    private void registerTraits() {
        if (this.setupEconomy()) {
            this.sendConsoleMessage("Registering Vault...");
            this.setupChat();
            this.setupPermissions();
        }
        if (Bukkit.getPluginManager().isPluginEnabled("Citizens")) {
            this.sendConsoleMessage("Registering Citizens traits...");
            CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(VendorTrait.class));
        }
    }

    private void sendConsoleMessage(String message) {
        Bukkit.getConsoleSender().sendMessage(String.valueOf(this.getPluginName()) + message);
    }

    private void setMythicMobFiles(File[] mythicMobFiles) {
        this.mythicMobFiles = mythicMobFiles;
    }

    private void setupCmds() {
        this.sendConsoleMessage("Setting up commands...");
        CommandHandler.setupCmds(this);
    }

    private boolean setupEconomy() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = this.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        this.econ = (Economy)rsp.getProvider();
        return this.econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = this.getServer().getServicesManager().getRegistration(Chat.class);
        this.chat = (Chat)rsp.getProvider();
        return this.chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = this.getServer().getServicesManager().getRegistration(Permission.class);
        this.perms = (Permission)rsp.getProvider();
        return this.perms != null;
    }

    public static Kolekce getCollection(String file) {
        return Utils.COLLECTIONS.getOrDefault(Kolekce.getNameFromFile(file), new Kolekce(file));
    }

    public static Kolekce getCollection(File file) {
        return DiabloLike.getCollection(file.getName());
    }

    public static DiabloItem getItemFromConfigName(String configName) {
        return Utils.DIABLO_ITEMS_BY_CONFIGNAME.get(configName);
    }

    public static List<DiabloItem> getItemFromDisplayName(String displayName) {
        return Utils.DIABLO_ITEMS_BY_DISPLAYNAME.get(ChatColor.stripColor((String)displayName));
    }

    public static Dungeon getDungeonFromName(String name) {
        return Utils.DUNGEONS.get(name);
    }
}

