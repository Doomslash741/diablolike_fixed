/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.TierColor;
import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.drops.groups.Kolekce;

public class Utils {
	private static DiabloLike plugin = DiabloLike.getInstance();
	public static List<File> FILES = new ArrayList<File>();
	public static Map<String, DiabloItem> DIABLO_ITEMS_BY_CONFIGNAME = new HashMap<String, DiabloItem>();
	public static List<String> DISPLAY_NAMES = new ArrayList<String>();
	public static Map<Integer, List<DiabloItem>> DROPS_PER_LEVEL = new HashMap<Integer, List<DiabloItem>>();
	public static Map<String, String> MOB_NAMES = new HashMap<String, String>();
	public static Map<String, List<DiabloItem>> DIABLO_ITEMS_BY_DISPLAYNAME = new HashMap<String, List<DiabloItem>>();
	public static HashMap<String, Kolekce> COLLECTIONS = new HashMap<String, Kolekce>();
	public static final List<Pattern> STAFF_LORE = Arrays.asList(Pattern.compile("Kouzlo:"),
			Pattern.compile("Po�kozen� kouzlem:"), Pattern.compile("Cooldown:"),
			Pattern.compile("Prav�m klikem �arujes"));
	public static Map<TierColor, Double> CONFIG_ATTRIBUTE_AMOUNT = new HashMap<TierColor, Double>();
	public static Map<TierColor, Double> GEM_CHANCES = new HashMap<TierColor, Double>();
	public static TreeMap<String, Dungeon> DUNGEONS = new TreeMap<String, Dungeon>();
	public static Map<String, String> DUNGEON_MOB_NAMES = new HashMap<String, String>();
	public static Map<File, List<DiabloItem>> LEATHER_ARMOR = new HashMap<File, List<DiabloItem>>();

	private static void loadFiles() {
		FILES = new ArrayList<File>();
		FILES.addAll(Arrays.asList(plugin.getCOLLECTIONS_FOLDER().listFiles()));
		FILES.addAll(Arrays.asList(plugin.getSpecificLootFolder().listFiles()));
		FILES.add(plugin.getItems());
	}

	private static void loadItems() {
		Bukkit.getConsoleSender().sendMessage(String.format("%sInitializing items...", plugin.getPluginName()));
		DIABLO_ITEMS_BY_CONFIGNAME = new HashMap<String, DiabloItem>();
		DIABLO_ITEMS_BY_DISPLAYNAME = new HashMap<String, List<DiabloItem>>();
		DROPS_PER_LEVEL = new HashMap<Integer, List<DiabloItem>>();
		DISPLAY_NAMES = new ArrayList<String>();
		LEATHER_ARMOR = new HashMap<File, List<DiabloItem>>();
		for (File file : FILES) {
			YamlConfiguration loader = YamlConfiguration.loadConfiguration(file);
			for (String key : loader.getKeys(false)) {
				DiabloItem item = new DiabloItem(key, loader);
				if (item.getItem() == null)
					continue;
				if (file == plugin.getItems()) {
					List<DiabloItem> levelDrops = DROPS_PER_LEVEL.containsKey(item.getLevel())
							? new ArrayList<DiabloItem>(DROPS_PER_LEVEL.get(item.getLevel()))
							: new ArrayList<>();
					levelDrops.add(item);
					DROPS_PER_LEVEL.put(item.getLevel(), levelDrops);
				}
				switch (item.getItem().getType()) {
				case LEATHER_HELMET:
				case LEATHER_CHESTPLATE:
				case LEATHER_LEGGINGS:
				case LEATHER_BOOTS: {
					ArrayList<DiabloItem> items = new ArrayList<DiabloItem>();
					if (LEATHER_ARMOR.containsKey(file)) {
						items.addAll(LEATHER_ARMOR.get(file));
					}
					items.add(item);
					LEATHER_ARMOR.put(file, items);
					break;
				}
				default:
					break;
				}
				DIABLO_ITEMS_BY_CONFIGNAME.put((String) key, item);
				String dName = ChatColor.stripColor((String) item.getDisplayName());
				DISPLAY_NAMES.add(dName);
				ArrayList<DiabloItem> byCfgName = new ArrayList<DiabloItem>();
				if (DIABLO_ITEMS_BY_DISPLAYNAME.containsKey(dName)) {
					byCfgName = new ArrayList<DiabloItem>(DIABLO_ITEMS_BY_DISPLAYNAME.get(dName));
				}
				byCfgName.add(item);
				DIABLO_ITEMS_BY_DISPLAYNAME.put(dName, byCfgName);
			}
		}
		Bukkit.getConsoleSender().sendMessage(String.format("%sInitializing collections...", plugin.getPluginName()));
		COLLECTIONS = new HashMap<String, Kolekce>();
		for (File file : plugin.getCOLLECTIONS_FOLDER().listFiles()) {
			Kolekce kolekce = new Kolekce(file);
			COLLECTIONS.put(kolekce.getName(), kolekce);
		}
		for (File file : plugin.getSpecificLootFolder().listFiles()) {
			Kolekce kolekce = new Kolekce(file);
			COLLECTIONS.put(kolekce.getName(), kolekce);
		}
		Bukkit.getConsoleSender().sendMessage(String.valueOf(plugin.getPluginName()) + (Object) ChatColor.GREEN
				+ "Loaded " + DIABLO_ITEMS_BY_CONFIGNAME.size() + " items and " + COLLECTIONS.size() + " collections.");
	}

	private static void loadConfig() {
		CONFIG_ATTRIBUTE_AMOUNT = new HashMap<TierColor, Double>();
		FileConfiguration cfg = plugin.getConfig();
		ConfigurationSection attrs = cfg.getConfigurationSection("pocetDiv");
		TierColor[] arrtierColor = TierColor.values();
		int n = arrtierColor.length;
		for (int i = 0; i < n; ++i) {
			TierColor tc = arrtierColor[i];
			CONFIG_ATTRIBUTE_AMOUNT.put(tc, attrs.getDouble(tc.toString()));
		}
		ConfigurationSection gemChance = cfg.getConfigurationSection("gem-chance");
		GEM_CHANCES = new HashMap<TierColor, Double>();
		for (TierColor tc : TierColor.values()) {
			GEM_CHANCES.put(tc, gemChance.getDouble(tc.toString()));
		}
	}

	public static void init() {
		Utils.loadFiles();
		Utils.loadItems();
		Utils.loadConfig();
	}

	private static List<DiabloItem> fromDisplayName(String displayName) {
		String name = ChatColor
				.stripColor((String) ChatColor.translateAlternateColorCodes((char) '&', (String) displayName));
		if (DIABLO_ITEMS_BY_DISPLAYNAME.containsKey(name)) {
			return DIABLO_ITEMS_BY_DISPLAYNAME.get(name);
		}
		return new ArrayList<DiabloItem>();
	}

	public static List<DiabloItem> fromDisplayName(ItemStack item) {
		List<DiabloItem> from;
		ArrayList<DiabloItem> items = new ArrayList<DiabloItem>();
		if (ItemUtils.isValid(item, true, false)
				&& !(from = Utils.fromDisplayName(item.getItemMeta().getDisplayName())).isEmpty()) {
			from.forEach(x -> {
				if (item.getType() == x.getItem().getType()) {
					items.add((DiabloItem) x);
				}
			});
		}
		return items;
	}
}
