/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package git.doomshade.diablolike.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class Config {
	protected FileConfiguration loader;
	protected InputStream is;
	protected File file;
	protected File directory;

	protected void setInputStream(InputStream is) {
		this.is = is;
	}

	protected void setFile(File file) {
		this.file = file;
	}

	protected void setDirectory(File directory) {
		this.directory = directory;
	}

	public void load() throws IOException {
		if (!this.directory.isDirectory()) {
			this.directory.mkdirs();
		}
		if (!this.file.exists()) {
			this.file.createNewFile();
			this.saveDefault();
		}
		this.loader = YamlConfiguration.loadConfiguration((File) this.file);
	}

	public FileConfiguration getLoader() {
		return this.loader;
	}

	public void saveDefault() throws FileNotFoundException, IOException {
		FileOutputStream fos = new FileOutputStream(this.file);
		try {
			try {
				int data;
				while ((data = this.is.read()) != -1) {
					fos.write(data);
				}
				fos.flush();
			} finally {
				fos.close();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
