/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.orbs.OrbType;

public class Initializer {
	public static void initFiles() {
		Initializer.initMobNames();
		Utils.init();
		Dungeon.initFiles();
		Initializer.initOrbs();
		
	}

	private static void initMobNames() {
		for (File file : new File("plugins/MythicMobs/Mobs").listFiles()) {
			YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) file);
			for (String s : loader.getKeys(false)) {
				if (!loader.getConfigurationSection(s).isString("Display"))
					continue;
				Utils.MOB_NAMES.put(ChatColor.stripColor((String) ChatColor.translateAlternateColorCodes((char) '&',
						(String) loader.getConfigurationSection(s).getString("Display"))), s);
			}
		}
	}

	private static void initOrbs() {
		YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) DiabloLike.getInstance().getOrbsFile());
		for (OrbType orb : OrbType.values()) {
			loader.set(ChatColor.stripColor((String) orb.getOrb().getOrbItem().getItemMeta().getDisplayName())
					.toLowerCase().replaceAll(" ", "_"), (Object) orb.getOrb().getOrbItem().serialize());
		}
		try {
			loader.save(DiabloLike.getInstance().getOrbsFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
