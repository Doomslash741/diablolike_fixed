/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.rit.sucy.CustomEnchantment
 *  com.rit.sucy.EnchantmentAPI
 *  javax.annotation.Nullable
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.rit.sucy.CustomEnchantment;
import com.rit.sucy.EnchantmentAPI;

import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.drops.Range;

public class DiabloItem {
	private Range amount;
	private int level;
	private double dropChance;
	private boolean isIdentified;
	private boolean hasDropChance;
	private String displayName;
	private String key;
	private List<String> lore;
	private ItemStack item;
	private Map<Chance, Double> additionalChance;
	private List<String> attributes;
	private static final Pattern POTREBNY_LVL = Pattern.compile(ItemUtils.POTREBNY_LVL);
	@Nullable
	private Map<CustomEnchantment, Range> customEnchantments = null;
	@Nullable
	private Map<Enchantment, Integer> enchantments = null;

	DiabloItem(String key, FileConfiguration loader) {
		this.key = key;
		this.isIdentified = false;
		if (!loader.isConfigurationSection(key)) {
			return;
		}
		ConfigurationSection section = loader.getConfigurationSection(key);
		if (section.isDouble("chance") || section.isInt("chance")) {
			this.dropChance = section.getDouble("chance");
			this.hasDropChance = true;
		} else if (loader.isDouble("chance") || loader.isInt("chance")) {
			this.dropChance = loader.getDouble("chance");
			this.hasDropChance = true;
		} else {
			this.dropChance = Chance.DROP.getChance();
			this.hasDropChance = false;
		}

		if (section.isInt("amount") || section.isString("amount")) {
			this.amount = Range.getFromString((String) section.get("amount"));
		} else {
			this.amount = new Range(1);
		}

		this.isIdentified = section.isBoolean("identified") ? section.getBoolean("identified") : false;
		this.item = ItemUtils.getItemFromFile(key, loader);
		this.displayName = this.item.getItemMeta().getDisplayName();
		this.lore = this.item.getItemMeta().getLore();
		this.level = Integer.MIN_VALUE;
		for (String s : this.lore) {
			if (!POTREBNY_LVL.matcher(s).find())
				continue;
			try {
				this.level = Integer.parseInt(
						ChatColor.stripColor((String) ChatColor.translateAlternateColorCodes((char) '&', (String) s))
								.replaceAll("\\D", ""));
				break;
			} catch (NumberFormatException e) {
				Bukkit.getConsoleSender()
						.sendMessage("Some mistake? (DiabloItem " + this.displayName + (Object) ChatColor.RESET + ")");
			}
		}
		this.additionalChance = new HashMap<Chance, Double>();
		for (Chance chance : Chance.values()) {
			String sChance = chance.toString();
			double ch = 0.0;
			boolean hasLoader = loader.isInt(sChance) || loader.isDouble(sChance);
			boolean hasSection = section.isInt(sChance) || section.isDouble(sChance);
			if (hasSection) {
				ch = section.getDouble(sChance);
				this.additionalChance.put(chance, ch);
			} else if (hasLoader) {
				ch = loader.getDouble(sChance);
				this.additionalChance.put(chance, ch);
			} else {
				this.additionalChance.put(chance, Double.valueOf(chance.getChance()));
			}
		}

		for (Entry<Chance, List<String>> entry : MainConfig.getCustomRarities().entrySet()) {
			for (String s : entry.getValue()) {
				if (ChatColor.stripColor(displayName).contains(s)) {
					additionalChance.put(entry.getKey(), 100d);
				}
			}
		}

		this.attributes = new ArrayList<String>();
		for (String s : this.lore) {
			if (MainConfig.getGems().contains(ChatColor.translateAlternateColorCodes((char) '&', (String) s))
					|| s.equalsIgnoreCase("\u00a72(M�sto pro klenot)")
					|| s.equalsIgnoreCase("\u00a78----------------------"))
				break;
			for (Pattern p : ItemUtils.sinisterAttrs) {
				if (p.matcher(ChatColor.stripColor(s)).find()) {
					this.attributes.add(s);
				}
			}
		}
		if (section.isConfigurationSection("enchantments")) {
			this.customEnchantments = new HashMap<CustomEnchantment, Range>();
			this.enchantments = new HashMap<Enchantment, Integer>();
			ConfigurationSection enchantmentSection = section.getConfigurationSection("enchantments");
			for (String enchantmentKey : enchantmentSection.getKeys(false)) {
				if (Enchantment.getByName((String) enchantmentKey) != null) {
					this.enchantments.put(Enchantment.getByName((String) enchantmentKey),
							enchantmentSection.getInt(enchantmentKey));
				}
				if (!Bukkit.getPluginManager().isPluginEnabled("EnchantmentAPI")
						|| !EnchantmentAPI.isRegistered((String) enchantmentKey))
					continue;
				CustomEnchantment ench = EnchantmentAPI.getEnchantment((String) enchantmentKey);
				Range range = enchantmentSection.isString(enchantmentKey)
						? Range.getFromString(enchantmentSection.getString(enchantmentKey))
						: new Range(enchantmentSection.getInt(enchantmentKey));
				this.customEnchantments.put(ench, range);
			}
		}
	}

	public boolean hasDropChance() {
		return this.hasDropChance;
	}

	public int getLevel() {
		return this.level;
	}

	public List<String> getAttributes() {
		return this.attributes;
	}

	public double getDropChance() {
		return this.dropChance;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public List<String> getLore() {
		return this.lore;
	}

	public boolean isAlwaysIdentified() {
		return this.isIdentified;
	}

	public Range getAmount() {
		return this.amount;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public ItemUtils getItemUtils() {
		return new ItemUtils(this.item);
	}

	public boolean hasLevel() {
		return this.getLevel() != Integer.MIN_VALUE;
	}

	public Map<Chance, Double> getAdditonalChances() {
		return this.additionalChance;
	}

	public Map<Enchantment, Integer> getEnchantments() {
		return this.enchantments;
	}

	public Map<CustomEnchantment, Range> getCustomEnchantments() {
		return this.customEnchantments;
	}

	public String getConfigName() {
		return this.key;
	}

	@Override
	public String toString() {
		return "DisplayName: " + (this.displayName == null ? "" : this.displayName) + " ConfigName: " + this.key
				+ " Additional Chances: " + this.additionalChance;
	}
}
