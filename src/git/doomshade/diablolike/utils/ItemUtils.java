/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.Material
 *  org.bukkit.SkullType
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 */
package git.doomshade.diablolike.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.TierColor;
import git.doomshade.diablolike.drops.Drop;
import git.doomshade.diablolike.drops.DropSettings;
import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.identifying.Identify;

public class ItemUtils {
	public static List<Pattern> sinisterAttrs = new ArrayList<Pattern>();
	private ItemStack item;
	private List<DiabloItem> diabloItems;
	public static String POTREBNY_LVL = "Pot�ebn� Lvl";

	static {
		sinisterAttrs.add(Pattern.compile("[+][0-9]+"));
		sinisterAttrs.add(Pattern.compile(": [0-9]+"));
		try {
			POTREBNY_LVL = YamlConfiguration.loadConfiguration(new File("plugins/SkillAPI/config.yml"))
					.getConfigurationSection("Items").getString("lore-level-text");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DiabloItem getDiabloItemFromFile(String s) {
		if (Utils.DIABLO_ITEMS_BY_CONFIGNAME.containsKey(s)) {
			return Utils.DIABLO_ITEMS_BY_CONFIGNAME.get(s);
		}
		return null;
	}

	public static ItemStack getItemFromFile(String s) {
		return ItemUtils.getDiabloItemFromFile(s).getItem();
	}

	public static ItemStack getItemFromFile(String key, FileConfiguration loader) {
		for (TierColor tc : TierColor.values()) {
			if (tc.toString().equalsIgnoreCase(key))
				return null;
		}
		if (!loader.isConfigurationSection(key)) {
			return null;
		}
		ConfigurationSection itemSection = loader.getConfigurationSection(key);
		if (itemSection.isConfigurationSection("item")) {
			return ItemStack
					.deserialize((Map<String, Object>) itemSection.getConfigurationSection("item").getValues(true));
		}
		int data = itemSection.isInt("data") ? itemSection.getInt("data") : 0;
		Color color = Color.fromRGB((int) itemSection.getInt("color"));
		try {
			return ItemUtils.makeItem(Material.getMaterial((String) itemSection.getString("materialName")),
					itemSection.getString("displayName"), itemSection.getStringList("lore"), (short) data, color);
		} catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage(key);
			return null;
		}
	}

	public static boolean isValid(ItemStack item) {
		return ItemUtils.isValid(item, true, true);
	}

	public static boolean isValid(ItemStack item, boolean withDisplayName, boolean withLore) {
		if (item == null) {
			return false;
		}
		if (withDisplayName || withLore) {
			if (!item.hasItemMeta()) {
				return false;
			}
			ItemMeta meta = item.getItemMeta();
			boolean hasDisplay = true;
			if (withDisplayName) {
				hasDisplay = meta.hasDisplayName();
			}
			
			boolean hasLore = true;
			if (withLore) {
				hasLore = meta.hasLore();
			}
			return hasDisplay && hasLore;
		}
		return true;
	}

	public static ItemStack makeItem(Material mat, String displayName, List<String> lore, short data) {
		return ItemUtils.makeItem(mat, displayName, lore, data, Color.AQUA);
	}

	public static ItemStack makeItem(Material mat, String displayName, List<String> lore, short data, Color color) {
		ItemStack item = new ItemStack(mat, 1, mat == Material.SKULL_ITEM ? (short) SkullType.PLAYER.ordinal() : data);
		if (ItemUtils.isLeather(mat)) {
			LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes((char) '&', (String) displayName));
			meta.setLore(ItemUtils.translateLore(lore));
			meta.setColor(color);
			item.setItemMeta((ItemMeta) meta);
		} else {
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes((char) '&', (String) displayName));
			meta.setLore(ItemUtils.translateLore(lore));
			item.setItemMeta(meta);
		}
		return item;
	}

	public static List<String> translateLore(List<String> lore) {
		ArrayList<String> newLore = new ArrayList<String>();
		lore.forEach(s -> {
			newLore.add(ChatColor.translateAlternateColorCodes((char) '&', (String) s));
		});
		return newLore;
	}

	private static boolean isLeather(Material mat) {
		switch (mat) {
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
			return true;
		default:
			break;
		}
		return false;
	}

	public ItemUtils(ItemStack item) {
		this.item = item;
		this.diabloItems = Utils.fromDisplayName(item);
	}

	public ItemUtils(DiabloItem item) {
		this(item.getItem());
	}

	public boolean alwaysDropItem() {
		return DropSettings.alwaysDrops.contains(ChatColor.stripColor((String) ChatColor
				.translateAlternateColorCodes((char) '&', (String) this.item.getItemMeta().getDisplayName())));
	}

	public boolean canBeDropped() {
		double dropChance = getDropChance();
		System.out.println(dropChance);
		return (Drop.random.nextDouble() * 100.0) <= dropChance;
	}

	public double getDropChance() {
		if (!diabloItems.isEmpty()) {
			return diabloItems.get(0).getDropChance();
		}
		return Chance.DROP.getChance();
	}

	public Map<Chance, Double> getAdditionalChance() {
		if (this.isValid(true, false) && !this.diabloItems.isEmpty()) {
			return diabloItems.get(0).getAdditonalChances();
		}
		HashMap<Chance, Double> map = new HashMap<Chance, Double>();
		for (Chance chance : Chance.values()) {
			map.put(chance, 0.0);
		}
		return map;
	}

	public boolean hasDropChance() {
		if (!this.diabloItems.isEmpty()) {
			return this.diabloItems.get(0).hasDropChance();
		}
		return false;
	}

	public List<String> getAttributes() {
		ItemMeta meta = this.item.getItemMeta();
		List<String> lore = meta.getLore();
		ArrayList<String> rLore = new ArrayList<String>();
		for (String s : lore) {
			if (MainConfig.getGems().contains(ChatColor.translateAlternateColorCodes((char) '&', (String) s))
					|| s.equalsIgnoreCase("\u00a72(M�sto pro klenot)")
					|| s.equalsIgnoreCase("\u00a78----------------------"))
				break;
			for (Pattern p : sinisterAttrs) {
				if (!p.matcher(ChatColor.stripColor((String) s)).find())
					continue;
				rLore.add(s);
			}
		}
		return rLore;
	}

	public ItemStack getDropItem() {
		return getDropItem(null);
	}

	public ItemStack getDropItem(Kolekce kolekce) {
		if (!this.isIdentifiable()) {
			System.out.println(isIdentifiable());
			System.out.println(item == null);
			return item;
		}
		this.item = setRandomRarity(kolekce);
		Identify id = new Identify(item);
		id.setIdentified(false);
		TierColor tc = this.getTierColor();
		if (DropSettings.identified.contains(ChatColor
				.stripColor(ChatColor.translateAlternateColorCodes('&', this.item.getItemMeta().getDisplayName())))
				|| tc == TierColor.COMMON || tc == TierColor.UNCOMMON) {
			id.setIdentified(true);
		}
		updatePrice();
		return this.item;
	}

	public ItemStack getDropItemMap(Map<Chance, Double> map) {
		if (!this.isValid(true, false) || !this.isIdentifiable()) {
			return this.item;
		}
		this.item = this.setRandomRarityMap(map);
		Identify id = new Identify(this.item);
		id.setIdentified(false);
		TierColor tc = this.getTierColor();
		if (DropSettings.identified.contains(ChatColor.stripColor((String) ChatColor
				.translateAlternateColorCodes((char) '&', (String) this.item.getItemMeta().getDisplayName())))
				|| tc == TierColor.COMMON || tc == TierColor.UNCOMMON) {
			id.setIdentified(true);

		}
		this.updatePrice();
		return this.item;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public int getItemLevel() {
		if (this.isValid(false, true) && !this.diabloItems.isEmpty()) {
			return this.diabloItems.get(0).getLevel();
		}
		return Integer.MIN_VALUE;
	}

	public boolean containsPrice() {
		if (!this.isValid()) {
			return false;
		}
		String priceLore = MainConfig.PRICE_LORE.replaceAll("%price",
				String.valueOf(MainConfig.getFormula().getSellPrice(this.item)));
		priceLore = ChatColor.translateAlternateColorCodes('&', priceLore);
		ItemMeta meta = this.item.getItemMeta();
		for (String s : meta.getLore()) {
			if (s == "" || !this.replacedPrice(s).contains(this.replacedPrice(priceLore)))
				continue;
			return true;
		}
		return false;
	}

	public double generateSellPrice() {
		if (!containsPrice()) {
			return 0.0;
		}
		return MainConfig.getFormula().getSellPrice(this.item);
	}

	public double getPrice() {
		if (!this.isValid()) {
			return 0;
		}
		String priceLore = MainConfig.PRICE_LORE.replaceAll("%price",
				String.valueOf(MainConfig.getFormula().getSellPrice(this.item)));
		priceLore = ChatColor.translateAlternateColorCodes('&', priceLore);
		ItemMeta meta = this.item.getItemMeta();
		for (String s : meta.getLore()) {
			if (s == "" || !replacedPrice(s).contains(replacedPrice(priceLore)))
				continue;
			String sLore = MainConfig.PRICE_LORE.replaceAll("%price", "");
			sLore = ChatColor.translateAlternateColorCodes('&', sLore);
			return Double.parseDouble(s.replaceAll(sLore, ""));
		}
		return 0;
	}

	public List<String> getRestOfLore() {
		ItemMeta meta = this.item.getItemMeta();
		List<String> lore = meta.getLore();
		ArrayList<String> rLore = new ArrayList<String>();
		boolean add = false;
		for (String s : lore) {
			if (!MainConfig.getGems().contains(ChatColor.translateAlternateColorCodes((char) '&', (String) s))
					&& !s.equalsIgnoreCase("\u00a78----------------------") && !add)
				continue;
			add = true;
			rLore.add(s);
		}
		return rLore;
	}

	public TierColor getTierColor() {
		for (TierColor tc : TierColor.values()) {
			if (!this.item.hasItemMeta() || !this.item.getItemMeta().hasDisplayName())
				break;
			if (!this.item.getItemMeta().getDisplayName().startsWith("\u00a7")
					|| !tc.getColor().equals(this.item.getItemMeta().getDisplayName().substring(0, 2)))
				continue;
			return tc;
		}
		return TierColor.COMMON;
	}

	public boolean isIdentifiable() {
		if (!isValid(false, true)) {
			return false;
		}
		for (String s : this.item.getItemMeta().getLore()) {
			if (ChatColor.stripColor(s).contains(POTREBNY_LVL))
				return true;
		}
		return false;
	}

	public boolean isSimilar(ItemStack to) {
		if (!this.isValid() || !isValid(to)) {
			return false;
		}
		return this.item.getType() == to.getType() && ChatColor
				.stripColor(ChatColor.translateAlternateColorCodes('&', this.item.getItemMeta().getDisplayName()))
				.equalsIgnoreCase(ChatColor
						.stripColor(ChatColor.translateAlternateColorCodes('&', to.getItemMeta().getDisplayName())));
	}

	public boolean isValid() {
		return isValid(this.item);
	}

	public boolean isValid(boolean withDisplayName, boolean withLore) {
		return isValid(this.item, withDisplayName, withLore);
	}

	public ItemStack setRandomRarity(Kolekce kolekce) {
		if (this.isValid()) {
			this.item = this.getRandomRarity(kolekce).setRarity(this.item);
		}
		return this.item;
	}

	public ItemStack setRandomRarityMap(Map<Chance, Double> map) {
		if (this.isValid()) {
			this.item = this.getRandomRarity(map).setRarity(this.item);
		}
		return this.item;
	}

	public void updatePrice() {
		if (!this.isValid()) {
			return;
		}
		removePreviousPrice();
		ItemMeta meta = this.item.getItemMeta();
		List<String> lore = meta.getLore();
		String priceLore = MainConfig.PRICE_LORE.replaceAll("%price",
				String.valueOf(MainConfig.getFormula().getSellPrice(this.item)));
		priceLore = ChatColor.translateAlternateColorCodes((char) '&', (String) priceLore);
		if (containsPrice()) {
			for (int i = 0; i < lore.size(); ++i) {
				if (!replacedPrice(lore.get(i)).contains(replacedPrice(priceLore)))
					continue;
				if (MainConfig.getFormula().getSellPrice(item) <= 0.0) {
					lore.remove(i);
				} else {
					lore.set(i, priceLore);
				}
				meta.setLore(lore);
				item.setItemMeta(meta);
				return;
			}
		}
		if (MainConfig.getFormula().getSellPrice(item) <= 0.0) {
			return;
		}
		int index = 0;
		Pattern lv = Pattern.compile(POTREBNY_LVL);
		for (String s : lore) {
			if (s == "") {
				++index;
				continue;
			}
			Matcher matcher = lv.matcher(ChatColor.stripColor((String) s));
			++index;
			if (matcher.find())
				break;
		}
		if (index >= lore.size() || index <= 0) {
			return;
		}
		lore.add(index, priceLore);
		meta.setLore(lore);
		this.item.setItemMeta(meta);
	}

	public TierColor getRandomRarity(Map<Chance, Double> map) {
		System.out.println(this.diabloItems.get(0).getConfigName());
		double mythic = map.get(Chance.MYTHIC);
		double legendary = map.get((Object) Chance.LEGENDARY) + mythic;
		double rare = map.get((Object) Chance.RARE) + legendary;
		double uncommon = map.get((Object) Chance.UNCOMMON) + rare;
		double random = Math.random() * 100.0;
		System.out.println(String.valueOf(mythic) + " " + random);
		
		if (random < mythic) {
			return TierColor.MYTHIC;
		}
		if (random < legendary) {
			return TierColor.LEGENDARY;
		}
		if (random < rare) {
			return TierColor.RARE;
		}
		if (random < uncommon) {
			return TierColor.UNCOMMON;
		}
		return TierColor.COMMON;
	}

	private TierColor getRandomRarity(Kolekce kolekce) {
		if (this.diabloItems.isEmpty()) {
			if (this.isValid(true, false)) {
				Bukkit.getConsoleSender().sendMessage(
						"(ItemUtils) No DiabloItem found with name " + this.item.getItemMeta().getDisplayName());
			}
			return TierColor.COMMON;
		}
		HashMap<Chance, Double> map = new HashMap<Chance, Double>(this.diabloItems.get(0).getAdditonalChances());
		if (kolekce != null) {
			System.out.println(kolekce.toString());
			for (Chance ch : kolekce.getAdditionalChances().keySet()) {
				map.put(ch, kolekce.getRarityChance(ch));
				System.out.println((Object) ch);
			}
		}
		return getRandomRarity(map);
	}

	private void removePreviousPrice() {
		if (!this.isValid()) {
			return;
		}
		ItemMeta meta = this.item.getItemMeta();
		List<String> lore = meta.getLore();
		for (int i = 0; i < lore.size() - 1; ++i) {
			if (lore.get(i) == null || lore.get(i) == "")
				continue;
			String col = ChatColor.stripColor((String) lore.get(i)).replaceAll("[0-9]+", "").replaceAll("[.]", "")
					.trim();
			for (String key : MainConfig.getPreviousLore()) {
				if (key == null || key == "")
					continue;
				String toCol = key.replaceAll("%price", "");
				toCol = ChatColor.translateAlternateColorCodes((char) '&', (String) toCol);
				if (!col.equalsIgnoreCase(toCol = ChatColor.stripColor((String) toCol).trim()))
					continue;
				lore.remove(i);
			}
		}
		meta.setLore(lore);
		this.item.setItemMeta(meta);
	}

	private String replacedPrice(String s) {
		return ChatColor.stripColor((String) s).replaceAll("[0-9]+", "").replaceAll("[.]", "");
	}
}
