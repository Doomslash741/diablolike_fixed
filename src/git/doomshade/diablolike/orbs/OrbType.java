/*
 * Decompiled with CFR 0.139.
 */
package git.doomshade.diablolike.orbs;

import git.doomshade.diablolike.orbs.Orb;
import git.doomshade.diablolike.orbs.orb.OrbLovcePriser;
import git.doomshade.diablolike.orbs.orb.OrbPrani;
import git.doomshade.diablolike.orbs.orb.OrbVylepseni2;
import java.util.HashMap;
import java.util.Map;

public enum OrbType {
    PRANI,
    LOVCE_PRISER,
    VYLEPSENI;
    
    private static Map<OrbType, Orb> ORBY;

    static {
        ORBY = new HashMap<OrbType, Orb>();
        ORBY.put(PRANI, new OrbPrani());
        ORBY.put(LOVCE_PRISER, new OrbLovcePriser());
        ORBY.put(VYLEPSENI, new OrbVylepseni2());
    }

    public Orb getOrb() {
        return ORBY.get((Object)this);
    }
}

