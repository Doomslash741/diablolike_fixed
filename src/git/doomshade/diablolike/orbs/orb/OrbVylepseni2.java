/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.orbs.orb;

import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.orbs.Orb;
import git.doomshade.diablolike.utils.ItemUtils;

public class OrbVylepseni2 extends Orb {
	private static Pattern vylepseniPattern;
	private static StringBuilder stringPattern;

	static {
		stringPattern = new StringBuilder("");
	}

	public OrbVylepseni2() {
		for (char c : ChatColor.stripColor((String) MainConfig.RAW_VYLEPSENI_LORE).toCharArray()) {
			try {
				Integer.parseInt(String.valueOf(c));
				stringPattern.append("[0-9]+");
			} catch (NumberFormatException e) {
				stringPattern.append(c);
			}
		}
		vylepseniPattern = Pattern.compile(stringPattern.toString());
	}

	@Override
	public ItemStack getOrbItem() {
		return OrbVylepseni2.makeOrbItem(Material.STONE, "&2Orb Vylepseni", "&aVylepsi dva nahodne staty o &61,0&a.");
	}

	@Override
	public boolean triggerOrb(ItemStack onItem) {
		ItemUtils utils = new ItemUtils(onItem);
		List<String> rest = utils.getRestOfLore();
		List<String> attrs = utils.getAttributes();
		ItemMeta meta = onItem.getItemMeta();
		int MAX = MainConfig.POCET_VYLEPSENI;
		for (int i = 0; i < MAX; ++i) {
			int rnd = new Random().nextInt(attrs.size());
			String barva = ChatColor.getLastColors((String) attrs.get(rnd));
			String cislo = ChatColor.stripColor((String) attrs.get(rnd));
			if (cislo.replaceAll("[^0-9]+", "") == "") {
				++MAX;
				continue;
			}
			try {
				int num = Integer.parseInt(cislo.replaceAll("[^0-9]+", "")) + 1;
				cislo = String.valueOf(barva) + cislo.replaceAll("[0-9]+", String.valueOf(num));
				attrs.set(rnd, cislo);
			} catch (NumberFormatException e) {
				++MAX;
			}
			if (MAX >= 20)
				break;
		}
		attrs.add("");
		attrs.addAll(rest);
		if (attrs.containsAll(meta.getLore())) {
			return false;
		}
		boolean hasVylepseni = false;
		for (int i = 0; i < attrs.size(); ++i) {
			String s = attrs.get(i);
			Matcher matcherVylepseni = vylepseniPattern.matcher(ChatColor.stripColor((String) s));
			if (!matcherVylepseni.find())
				continue;
			hasVylepseni = true;
			int currVylepseni = 0;
			try {
				currVylepseni = Integer.parseInt(ChatColor.stripColor((String) s).replaceAll("[^0-9]+", "").trim()
						.replaceAll(String.valueOf(MainConfig.MAX_VYLEPSENI), ""));
			} catch (NumberFormatException e) {
				return false;
			}
			attrs.set(i, OrbVylepseni2.vylepseniToLore(currVylepseni + 1));
			break;
		}
		if (!hasVylepseni) {
			attrs.add(OrbVylepseni2.vylepseniToLore(1));
		}
		meta.setLore(attrs);
		onItem.setItemMeta(meta);
		return true;
	}

	private static String vylepseniToLore(int pocetVylepseni) {
		return ChatColor.translateAlternateColorCodes((char) '&',
				(String) MainConfig.VYLEPSENI_LORE.replaceAll("%curr", String.valueOf(pocetVylepseni))
						.replaceAll("%max", String.valueOf(MainConfig.MAX_VYLEPSENI)));
	}

	public static int pocetVylepseni(ItemStack onItem) {
		Pattern p = Pattern.compile("[0-9]+/[0-9]+");
		Matcher m = p.matcher(onItem.getItemMeta().getDisplayName());
		int pocetVylepseni = 0;
		if (m.find()) {
			try {
				pocetVylepseni = Integer.parseInt(m.group().substring(0, 1));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return pocetVylepseni;
	}
}
