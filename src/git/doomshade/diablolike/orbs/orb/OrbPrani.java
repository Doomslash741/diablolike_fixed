/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.orbs.orb;

import git.doomshade.diablolike.identifying.Identify;
import git.doomshade.diablolike.orbs.Orb;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class OrbPrani
extends Orb {
    @Override
    public ItemStack getOrbItem() {
        return Orb.makeOrbItem(Material.STONE, "&a&oOrb Prani", "&a&oOdstrani vsechny atributy predmetu.", "&a&oPredmet se premeni na &5&lneidentifikovany&a&o.");
    }

    @Override
    public boolean triggerOrb(ItemStack onItem) {
        Identify i = new Identify(onItem);
        if (i.isIdentified()) {
            i.setIdentified(false);
            return true;
        }
        return false;
    }
}

