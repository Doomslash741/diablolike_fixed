/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.configuration.serialization.ConfigurationSerializable
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.orbs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.config.MainConfig;

public abstract class Orb implements ConfigurationSerializable {
	@SuppressWarnings("unchecked")
	public static /* varargs */ ItemStack makeOrbItem(Material mat, String displayName, String... s) {
		ItemStack orb = new ItemStack(mat);
		ItemMeta meta = orb.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes((char) '&', (String) displayName));
		ArrayList<String> lore = new ArrayList<String>();
		for (String st : s) {
			lore.add(ChatColor.translateAlternateColorCodes((char) '&', (String) st));
		}
		try {
			lore.addAll((List<String>) MainConfig.ORB_LORE.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
		meta.setLore(lore);
		orb.setItemMeta(meta);
		return orb;
	}

	public abstract ItemStack getOrbItem();

	public abstract boolean triggerOrb(ItemStack var1);

	@SuppressWarnings("unused")
	public static boolean isOrb(ItemStack item) {
		return true;
	}

	@Override
	public Map<String, Object> serialize() {
		return null;
	}

	@SuppressWarnings("unused")
	public static Orb deserialize(Map<String, Object> map) {
		return null;
	}
}
