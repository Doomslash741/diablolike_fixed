/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 */
package git.doomshade.diablolike.cmds;

import git.doomshade.diablolike.DiabloLike;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public abstract class AbstractCommand {
    public static List<AbstractCommand> CMDS = new ArrayList<AbstractCommand>();
    protected DiabloLike plugin;

    public AbstractCommand(DiabloLike plugin) {
        this.plugin = plugin;
        for (AbstractCommand acmd : CMDS) {
            if (!acmd.getCommand().equalsIgnoreCase(this.getCommand())) continue;
            return;
        }
        CMDS.add(this);
    }

    public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

    public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);

    protected abstract Map<Boolean, List<String>> getArgs();

    public abstract String getCommand();

    protected abstract String getDescription();

    protected abstract boolean requiresPlayer();

    public abstract boolean requiresOp();
}

