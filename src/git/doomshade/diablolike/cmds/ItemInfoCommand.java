package git.doomshade.diablolike.cmds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.identifying.Identify;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class ItemInfoCommand extends AbstractCommand {
	public static String MENUNAME = "\u00a72Informace o predmetu";
	private static final String CHECKMARK = ChatColor.GREEN + "✔";
	private static final String CROSS = ChatColor.RED + "✘";

	@SuppressWarnings("unused")
	private static ItemStack glass() {
		ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("\u00a7a");
		item.setItemMeta(meta);
		return item;
	}

	private static Inventory menu(List<ItemStack> items) {
		Inventory inv = Bukkit.createInventory(null, (int) 9, (String) MENUNAME);
		ItemStack[] contents = new ItemStack[inv.getSize()];
		for (int i = 0; i < items.size(); ++i) {
			contents[i] = items.get(i);
		}
		inv.setContents(contents);
		return inv;
	}

	public ItemInfoCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		ItemStack mh = hrac.getInventory().getItemInMainHand();
		if (mh == null) {
			hrac.sendMessage(Messages.NO_ITEM_IN_HAND.getMessage());
			return true;
		}
		ItemUtils utils = new ItemUtils(mh);
		if (!utils.isValid(true, false)) {
			hrac.sendMessage(Messages.CANNOT_GIVE_INFO.getMessage(mh.getType().toString()));
			return true;
		}
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		for (DiabloItem item : Utils.DIABLO_ITEMS_BY_CONFIGNAME.values()) {
			int i;
			utils = new ItemUtils(item.getItem());
			if (!utils.isValid(true, false) || !utils.isSimilar(mh))
				continue;
			ItemStack copy = item.getItem().clone();
			ItemMeta meta = copy.getItemMeta();
			meta.setDisplayName(mh.getItemMeta().getDisplayName());
			List<String> mhLore = mh.getItemMeta().getLore();
			List<String> configLore = meta.getLore();
			HashMap<Integer, Integer> indexes = new HashMap<Integer, Integer>();
			ArrayList<Integer> configNums = new ArrayList<Integer>();
			for (int i2 = 0; i2 < configLore.size(); ++i2) {
				String cfgLoreS = ChatColor.stripColor((String) ((String) configLore.get(i2))).replaceAll("\\d", "")
						.replaceAll("-", "");
				if (Identify.isAttribute((String) configLore.get(i2))) {
					configNums.add(Integer.parseInt(ChatColor
							.stripColor((String) ItemInfoCommand.replaceStringWith((String) configLore.get(i2), true))
							.replaceAll("\\D", "")));
				}
				for (int o = 0; o < mhLore.size(); ++o) {
					if (!Identify.isAttribute((String) mhLore.get(o)) || !cfgLoreS.equalsIgnoreCase(
							ChatColor.stripColor((String) ((String) mhLore.get(o))).replaceAll("\\d", "")))
						continue;
					indexes.put(i2, o);
				}
			}
			ArrayList<Integer> mhNums = new ArrayList<Integer>();
			Iterator<Integer> o = indexes.values().iterator();
			while (o.hasNext()) {
				int i3 = (Integer) o.next();
				try {
					int num = Integer
							.parseInt(ChatColor.stripColor((String) ((String) mhLore.get(i3))).replaceAll("\\D", ""));
					mhNums.add(num);
				} catch (Exception num) {
					// empty catch block
				}
			}
			double configNumSum = 0.0;
			Iterator<Integer> iterator = configNums.iterator();
			while (iterator.hasNext()) {
				int i4 = (Integer) iterator.next();
				configNumSum += (double) i4;
			}
			double mhNumSum = 0.0;
			Iterator<Integer> iterator2 = mhNums.iterator();
			while (iterator2.hasNext()) {
				i = (Integer) iterator2.next();
				mhNumSum += (double) i;
			}
			if (configNumSum != 0.0) {
				double perfection = (double) Math.round(mhNumSum / configNumSum * 10000.0) / 100.0;
				meta.setDisplayName(String.valueOf(meta.getDisplayName()) + " - " + perfection + "%");
			}
			for (i = 0; i < configLore.size(); ++i) {
				String configString = (String) configLore.get(i);
				if (configString.isEmpty() || configString == "" || !Identify.isAttribute(configString))
					continue;
				if (configString.contains("-----") || configString.contains("Misto pro klenot"))
					break;
				int configNum = 0;
				try {
					configNum = Integer.parseInt(
							ChatColor.stripColor((String) ItemInfoCommand.replaceStringWith(configString, true).trim())
									.replaceAll("\\D", ""));
				} catch (Exception e) {
					continue;
				}
				int mhNum = 0;
				if (indexes.keySet().contains(i)) {
					try {
						mhNum = Integer
								.parseInt(ChatColor.stripColor((String) ((String) mhLore.get((Integer) indexes.get(i))))
										.replaceAll("\\D", ""));
					} catch (Exception exception) {
						// empty catch block
					}
				}
				int difference = mhNum - configNum;
				ChatColor color = ChatColor.RED;
				if (difference == 0) {
					color = null;
				} else if (difference > 0) {
					color = ChatColor.GREEN;
				}
				StringBuffer sb = new StringBuffer(configString);
				sb.append(" | ");
				if (mhNum == 0) {
					sb.append(CROSS);
				} else {
					sb.append(color != null ? (Object) color + String.valueOf(difference) : CHECKMARK);
				}
				configLore.set(i, sb.toString());
			}
			meta.setLore(configLore);
			copy.setItemMeta(meta);
			items.add(copy);
		}
		if (items.isEmpty()) {
			hrac.sendMessage(Messages.CANNOT_GIVE_INFO.getMessage(mh.getItemMeta().getDisplayName()));
			return true;
		}
		hrac.openInventory(ItemInfoCommand.menu(items));
		return true;
	}

	private static String replaceStringWith(String s, boolean isMax) {
		String rangeOnly = "[^\\d-\u00a7]+";
		String strNumRange = s.replaceAll(rangeOnly, "");
		String regex = "\u00a7?[0-9]+[-][0-9]+";
		strNumRange = ChatColor.stripColor((String) s).replaceAll("[^\\d-]", "");
		if (strNumRange.matches(regex)) {
			String[] split = strNumRange.split("-");
			int num = 0;
			Range range = new Range(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
			num = isMax ? range.getMax() : range.getMin();
			String rn = String.valueOf(num);
			return s.replaceAll(regex, rn);
		}
		return s;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		return null;
	}

	@Override
	public String getCommand() {
		return "info";
	}

	@Override
	protected String getDescription() {
		return "zobrazi mozne staty";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return false;
	}
}
