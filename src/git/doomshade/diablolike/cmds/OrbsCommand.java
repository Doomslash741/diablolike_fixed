/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package git.doomshade.diablolike.cmds;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.orbs.OrbType;

public class OrbsCommand extends AbstractCommand {
	public OrbsCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		int amount = 1;
		if (args.length == 2) {
			try {
				amount = Integer.getInteger(args[1]);
			} catch (Exception exception) {
				// empty catch block
			}
		}
		for (int i = 0; i < amount; ++i) {
			for (OrbType ot : OrbType.values()) {
				hrac.getInventory().addItem(new ItemStack[] { ot.getOrb().getOrbItem() });
			}
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(false, Arrays.asList("amount"));
		return map;
	}

	@Override
	public String getCommand() {
		return "giveorbs";
	}

	@Override
	protected String getDescription() {
		return "da ti vsechny orby";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
