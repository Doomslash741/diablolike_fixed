/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package git.doomshade.diablolike.cmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.utils.Utils;

public class KolekceCommand extends AbstractCommand {
	public KolekceCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		if (args.length == 1) {
			hrac.sendMessage(CommandHandler.infoMessage(this));
			return true;
		}

		// 1. arg <kolekce>
		Kolekce kolekce = DiabloLike.getCollection(args[1]);
		if (!kolekce.exists()) {
			hrac.sendMessage("Tato kolekce neexistuje");
			return true;
		}

		// 2. arg [hrac]
		final Player to;
		if (args.length >= 3) {
			to = Bukkit.getPlayer(args[2]);
			if (!to.isValid()) {
				hrac.sendMessage("Neplatny hrac");
				return true;
			}
		} else {
			to = hrac;
		}
		
		// 3. arg [chance]
		kolekce.setChance(100);
		if (args.length >= 4) {
			kolekce.setChance(Integer.parseInt(args[3]));
		}
		
		// 4. arg [amount]
		Range range = new Range(1);
		if (args.length >= 5) {
			range = Range.getFromString(args[4]);
		}
		
		// 5.-7. arg [tiercolor]
		LinkedList<Double> chances = new LinkedList<Double>();
		for (int i = 5; i < args.length && i != 8; ++i) {
			chances.add(Double.parseDouble(args[i]));
		}
		if (!chances.isEmpty()) {
			kolekce.setRarityChance(Chance.LEGENDARY, (Double) chances.removeFirst());
		}
		if (!chances.isEmpty()) {
			kolekce.setRarityChance(Chance.RARE, (Double) chances.removeFirst());
		}
		if (!chances.isEmpty()) {
			kolekce.setRarityChance(Chance.UNCOMMON, (Double) chances.removeFirst());
		}
		
		kolekce.setAmount(range);
		List<ItemStack> drop = kolekce.getRandomDrop();
		drop.forEach(x -> to.getInventory().addItem(x));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		ArrayList<String> tab = new ArrayList<String>();
		switch (args.length) {
		case 1: {
			tab.addAll(Utils.COLLECTIONS.keySet());
			return tab;
		}
		case 2: {
			for (Entry<String, Kolekce> a : Utils.COLLECTIONS.entrySet()) {
				if (a.getKey().startsWith(args[1])) {
					tab.add(a.getKey());
				}
			}
			return tab;
		}
		}
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(true, Arrays.asList("kolekce"));
		map.put(false, Arrays.asList("player", "chance", "amount", "legendary", "rare", "uncommon"));
		return map;
	}

	@Override
	public String getCommand() {
		return "kolekce";
	}

	@Override
	protected String getDescription() {
		return "givne kolekci";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
