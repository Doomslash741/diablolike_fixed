/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.SkullMeta
 */
package git.doomshade.diablolike.cmds;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.utils.Utils;

public class AddCommand extends AbstractCommand {
	public AddCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		String configName = args[1];
		File file = null;
		switch (args.length) {
		case 2: {
			file = DiabloLike.getInstance().getItems();
			break;
		}
		case 3: {
			for (File filee : Utils.FILES) {
				if (!filee.getName().equalsIgnoreCase(args[2]))
					continue;
				file = filee;
				break;
			}
		}
		}
		if (file == null) {
			hrac.sendMessage("Nenasel se file se jmenem " + args[2] + ".");
			return true;
		}
		YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) file);
		if (loader.isConfigurationSection(configName)) {
			hrac.sendMessage(String.valueOf(configName) + " config jmeno je jiz obsazeno.");
			return true;
		}
		ItemStack item = hrac.getInventory().getItemInMainHand();
		if (item == null) {
			hrac.sendMessage("Musis mit v ruce nejaky predmet");
			return true;
		}
		ConfigurationSection section = loader.createSection(configName);
		if (item.hasItemMeta() && item.getItemMeta() instanceof SkullMeta) {
			section.set("item", (Object) item.serialize());
		} else {
			section.set("materialName", (Object) item.getType().toString().toUpperCase());
			section.set("displayName", (Object) "");
			section.set("lore", Arrays.asList(""));
			if (item.hasItemMeta()) {
				ItemMeta meta = item.getItemMeta();
				if (meta.hasDisplayName()) {
					section.set("displayName", (Object) meta.getDisplayName().replaceAll("\u00a7", "&"));
				}
				if (meta.hasLore()) {
					ArrayList<String> lore = new ArrayList<String>(meta.getLore());
					for (int i = 0; i < lore.size(); ++i) {
						lore.set(i, ((String) lore.get(i)).replaceAll("\u00a7", "&"));
					}
					section.set("lore", lore);
				}
			}
		}
		try {
			loader.save(file);
			hrac.sendMessage("Predmet ulozen do " + file.getName());
		} catch (IOException e) {
			e.printStackTrace();
			hrac.sendMessage("Chyba v ukladani predmetu. Checkni konzoli!");
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		switch (args.length) {
		case 2: {
			Player hrac = (Player) sender;
			ItemStack item = hrac.getInventory().getItemInMainHand();
			if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName())
				break;
			list.add(ChatColor.stripColor((String) item.getItemMeta().getDisplayName()).toLowerCase().replaceAll(" ",
					"_"));
			break;
		}
		case 3: {
			for (File file : Utils.FILES) {
				list.add(file.getName());
			}
			break;
		}
		case 4: {
			for (File file : Utils.FILES) {
				String fileName = file.getName();
				if (!fileName.startsWith(args[2]))
					continue;
				list.add(fileName);
			}
		}
		}
		return list.isEmpty() ? null : list;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(true, Arrays.asList("config name"));
		map.put(false, Arrays.asList("file"));
		return map;
	}

	@Override
	public String getCommand() {
		return "add";
	}

	@Override
	protected String getDescription() {
		return "prida item do systemu";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
