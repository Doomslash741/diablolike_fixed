package git.doomshade.diablolike.cmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class CustomCommand extends AbstractCommand {
	public CustomCommand(DiabloLike plugin) {
		super(plugin);
	}

	public static final Map<UUID, Inventory> INVENTORIES = new HashMap<UUID, Inventory>();
	private static final Map<UUID, Integer> DURATIONS = new HashMap<UUID, Integer>();
	public static final String INV_PREFIX = "Prebytecne predmety - ";
	private static final Map<UUID, BukkitTask> RUNNABLES = new HashMap<UUID, BukkitTask>();

	private Inventory createMenu(final Player hrac, ItemStack... items) {
		int size = (items.length / 9 + 1) * 9;
		Inventory inv = null;
		if (INVENTORIES.containsKey(hrac.getUniqueId())) {
			int prevInvSize = 0;
			if (((Inventory) INVENTORIES.get(hrac.getUniqueId())).firstEmpty() == -1) {
				prevInvSize += 9;
			}
			while (((Inventory) INVENTORIES.get(hrac.getUniqueId())).firstEmpty() % 9 + items.length
					- prevInvSize > 9) {
				prevInvSize += 9;
			}
			inv = Bukkit.createInventory(null,
					((Inventory) INVENTORIES.get(hrac.getUniqueId())).getSize() + prevInvSize,
					INV_PREFIX + hrac.getDisplayName());
			for (ItemStack item : ((Inventory) INVENTORIES.get(hrac.getUniqueId())).getStorageContents()) {
				if (item != null) {
					inv.addItem(new ItemStack[] { item });
				}
			}
		} else {
			inv = Bukkit.createInventory(null, size, INV_PREFIX + hrac.getDisplayName());
		}
		inv.addItem(items);
		INVENTORIES.put(hrac.getUniqueId(), inv);
		DURATIONS.put(hrac.getUniqueId(), Integer.valueOf(120));
		BukkitRunnable br = new BukkitRunnable() {
			@Override
			public void run() {
				if (((Integer) CustomCommand.DURATIONS.get(hrac.getUniqueId())).intValue() <= 0) {
					CustomCommand.INVENTORIES.remove(hrac.getUniqueId());
					CustomCommand.RUNNABLES.remove(hrac.getUniqueId());
					cancel();
				}

				switch (((Integer) CustomCommand.DURATIONS.get(hrac.getUniqueId())).intValue()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 15:
				case 30:
				case 45:
				case 60:
				case 90:
				case 120:
					hrac.sendMessage(
							ChatColor.RED + "Zbyva " + ChatColor.GOLD + CustomCommand.DURATIONS.get(hrac.getUniqueId())
									+ " vterin" + ChatColor.RED + " do vyprchani prebytecnych predmetu.");
				}

				CustomCommand.DURATIONS.put(hrac.getUniqueId(),
						Integer.valueOf(((Integer) CustomCommand.DURATIONS.get(hrac.getUniqueId())).intValue() - 1));
			}

		};
		BukkitTask task = br.runTaskTimer(plugin, 0L, 20L);

		if (RUNNABLES.containsKey(hrac.getUniqueId())) {
			((BukkitTask) RUNNABLES.get(hrac.getUniqueId())).cancel();
		}
		RUNNABLES.put(hrac.getUniqueId(), task);
		return inv;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = null;
		if ((sender instanceof Player))
			hrac = (Player) sender;
		if (args.length == 1) {
			sender.sendMessage(CommandHandler.infoMessage(this));
			return true;
		}
		boolean identified = true;
		if (args.length >= 5) {
			try {
				identified = Boolean.parseBoolean(args[4]);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		int amount = 1;
		if (args.length >= 4) {
			try {
				amount = Integer.parseInt(args[3]);
			} catch (NumberFormatException localNumberFormatException) {
			}
		}

		if (args.length >= 3) {
			try {
				hrac = Bukkit.getPlayer(args[2]);
				if (hrac == null || !hrac.isValid()) {
					sender.sendMessage(
							plugin.toString() + String.format("Invalid player name! (%s)", new Object[] { args[3] }));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (hrac == null) {
			return false;
		}
		String configItem = args[1];

		if (!Utils.DIABLO_ITEMS_BY_CONFIGNAME.containsKey(configItem)) {
			sender.sendMessage("P�edm�t s t�mto cfg jm�nem neexistuje");
			return true;
		}
		DiabloItem item = ItemUtils.getDiabloItemFromFile(configItem);
		ItemUtils utils = item.getItemUtils();
		List<ItemStack> exceededItems = new ArrayList<ItemStack>();
		for (int i = 0; i < amount; i++) {
			if (hrac.getInventory().firstEmpty() == -1) {
				if (identified) {
					exceededItems.add(utils.getDropItem());
				} else {
					exceededItems.add(item.getItem());
				}
			} else if (identified) {
				hrac.getInventory().addItem(utils.getDropItem());
			} else
				hrac.getInventory().addItem(item.getItem());
		}
		hrac.sendMessage(Messages.ITEM_GIVEN.getMessage(utils.getItem().getItemMeta().getDisplayName()));
		if (!exceededItems.isEmpty()) {
			hrac.sendMessage(ChatColor.GREEN + "Pokud si chces preorganizovat svuj inventar, napis pote tento prikaz: "
					+ CommandHandler.infoMessage(new InventoryCommand(plugin)));
			hrac.openInventory(createMenu(hrac, (ItemStack[]) exceededItems.toArray(new ItemStack[0])));
		}
		if (sender != hrac)
			sender.sendMessage(hrac.getDisplayName() + " ziskal " + amount + "x "
					+ utils.getItem().getItemMeta().getDisplayName());
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			return Arrays.asList((String[]) Utils.DIABLO_ITEMS_BY_CONFIGNAME.keySet().toArray(new String[0]));
		}

		if (args.length == 2) {
			List<String> items = new ArrayList<String>();
			for (String s : Utils.DIABLO_ITEMS_BY_CONFIGNAME.keySet()) {
				if (s.startsWith(args[1])) {
					items.add(s);
				}
			}
			return items;
		}
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		Map<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(Boolean.valueOf(true), Arrays.asList(new String[] { "item" }));
		map.put(Boolean.valueOf(false), Arrays.asList(new String[] { "player", "amount", "identified" }));
		return map;
	}

	@Override
	public String getCommand() {
		return "custom";
	}

	@Override
	protected String getDescription() {
		return "givne item na zaklade config jmena";
	}

	@Override
	protected boolean requiresPlayer() {
		return false;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
