/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 */
package git.doomshade.diablolike.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import git.doomshade.diablolike.DiabloLike;

public class InventoryCommand extends AbstractCommand {
	public InventoryCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		if (!CustomCommand.INVENTORIES.containsKey(hrac.getUniqueId())) {
			return true;
		}
		hrac.openInventory(CustomCommand.INVENTORIES.get(hrac.getUniqueId()));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		return null;
	}

	@Override
	public String getCommand() {
		return "inv";
	}

	@Override
	protected String getDescription() {
		return "zobrazi ti inventar s prebytecnymi predmety";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return false;
	}
}
