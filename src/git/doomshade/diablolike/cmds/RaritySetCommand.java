/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.cmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.TierColor;
import git.doomshade.diablolike.identifying.Identify;

public class RaritySetCommand extends AbstractCommand {
	public RaritySetCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		ItemStack item = hrac.getInventory().getItemInMainHand();
		if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName() || args.length < 2) {
			return false;
		}
		ItemMeta meta = item.getItemMeta();
		TierColor barva = TierColor.fromName(args[1]);
		if (barva == null) {
			return false;
		}
		meta.setDisplayName(String.valueOf(barva.getColor()) + ChatColor.stripColor((String) meta.getDisplayName()));
		item.setItemMeta(meta);
		new Identify(item).reidentify();
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			ArrayList<String> tab = new ArrayList<String>();
			for (TierColor tc : TierColor.values()) {
				tab.add(tc.toString().toLowerCase());
			}
			return tab;
		}
		if (args.length == 2) {
			ArrayList<String> tab = new ArrayList<String>();
			for (TierColor tc : TierColor.values()) {
				if (!tc.toString().toLowerCase().startsWith(args[1].toLowerCase()))
					continue;
				tab.add(tc.toString().toLowerCase());
			}
			return tab;
		}
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> args = new HashMap<Boolean, List<String>>();
		args.put(true, Arrays.asList("rarity"));
		return args;
	}

	@Override
	public String getCommand() {
		return "setrarity";
	}

	@Override
	protected String getDescription() {
		return "sets the rarity of an item";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
