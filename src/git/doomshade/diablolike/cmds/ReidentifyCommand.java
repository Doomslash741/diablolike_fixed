/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package git.doomshade.diablolike.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.identifying.Identify;
import git.doomshade.diablolike.utils.ItemUtils;

public class ReidentifyCommand extends AbstractCommand {
	public ReidentifyCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		ItemStack item = hrac.getInventory().getItemInMainHand();
		if (item == null || !new ItemUtils(item).isValid()) {
			return false;
		}
		new Identify(item).reidentify();
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		return null;
	}

	@Override
	public String getCommand() {
		return "reidentify";
	}

	@Override
	protected String getDescription() {
		return "znovu identifikuje predmet";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
