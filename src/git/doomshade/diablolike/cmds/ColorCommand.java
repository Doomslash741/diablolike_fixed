/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package git.doomshade.diablolike.cmds;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.Utils;

public class ColorCommand extends AbstractCommand {
	public ColorCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Pattern cfgName = Pattern.compile(args[1]);
		Color color = null;
		try {
			color = Color.fromRGB(Integer.parseInt(args[2].replaceAll("#", ""), 16));
		} catch (Exception e) {
			sender.sendMessage("Spatne napsana barva.");
			return false;
		}
		ArrayList<String> cfgJmena = new ArrayList<String>();
		ArrayList<String> itemJmena = new ArrayList<String>();
		for (File file : Utils.LEATHER_ARMOR.keySet()) {
			YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) file);
			for (DiabloItem item : Utils.LEATHER_ARMOR.get(file)) {
				String name = ChatColor.stripColor(item.getDisplayName());
				if (item.getDisplayName() == null && item.getDisplayName().isEmpty()
						|| !cfgName.matcher(name).find())
					continue;
				ConfigurationSection section = loader.getConfigurationSection(item.getConfigName());
				section.set("color", color.asRGB());
				cfgJmena.add(item.getConfigName());
				itemJmena.add(item.getDisplayName());
			}
			try {
				loader.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < cfgJmena.size(); ++i) {
			sender.sendMessage("Obarven " + (String) itemJmena.get(i) + (Object) ChatColor.RESET + " s config jmenem "
					+ (String) cfgJmena.get(i));
		}
		sender.sendMessage("Na konci napis /dl reload, aby se itemy znovu nacetly");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(true, Arrays.asList("display_jmeno", "color_code"));
		return map;
	}

	@Override
	public String getCommand() {
		return "color";
	}

	@Override
	protected String getDescription() {
		return "obarvi vsechny leathery zacinajici s urcitym cfg jmenem";
	}

	@Override
	protected boolean requiresPlayer() {
		return false;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
