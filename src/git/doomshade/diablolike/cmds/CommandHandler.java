/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.command.PluginCommand
 *  org.bukkit.command.TabCompleter
 *  org.bukkit.entity.Player
 */
package git.doomshade.diablolike.cmds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import git.doomshade.diablolike.DiabloLike;

public class CommandHandler implements CommandExecutor, TabCompleter {
	private static DiabloLike plugin = DiabloLike.getInstance();
	private static final PluginCommand cmd = plugin.getCommand("dl");

	private CommandHandler() {
	}

	private static boolean isValid(CommandSender sender, AbstractCommand acmd) {
		return (!acmd.requiresOp() || sender.isOp()) && (!acmd.requiresPlayer() || sender instanceof Player);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage(ChatColor.DARK_AQUA + "\u00a7m-------" + ChatColor.DARK_AQUA + "["
					+ (Object) ChatColor.RED + plugin.getName() + ChatColor.DARK_AQUA + "]\u00a7m-------");
			String message = null;
			for (AbstractCommand acmd : AbstractCommand.CMDS) {
				if (!CommandHandler.isValid(sender, acmd) || (message = CommandHandler.infoMessage(acmd)) == null)
					continue;
				sender.sendMessage(message);
			}
			return true;
		}
		for (AbstractCommand acmd : AbstractCommand.CMDS) {
			if (!acmd.getCommand().equalsIgnoreCase(args[0]))
				continue;
			if (!CommandHandler.isValid(sender, acmd)) {
				return false;
			}
			List<String> cmdArgs = acmd.getArgs() != null && acmd.getArgs().containsKey(true) ? acmd.getArgs().get(true)
					: null;
			if (cmdArgs != null && cmdArgs.size() > args.length - 1) {
				sender.sendMessage(CommandHandler.infoMessage(acmd));
				return true;
			}
			acmd.onCommand(sender, cmd, label, args);
			return true;
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		ArrayList<String> tab = new ArrayList<String>();
		if (args.length == 0) {
			AbstractCommand.CMDS.forEach(x -> {
				if (CommandHandler.isValid(sender, x)) {
					tab.add(x.getCommand());
				}
			});
			return tab;
		}
		for (AbstractCommand acmd : AbstractCommand.CMDS) {
			if (!CommandHandler.isValid(sender, acmd))
				continue;
			if (acmd.getCommand().equalsIgnoreCase(args[0])) {
				return acmd.onTabComplete(sender, cmd, label, args);
			}
			if (!acmd.getCommand().startsWith(args[0]))
				continue;
			tab.add(acmd.getCommand());
		}
		return tab.isEmpty() ? null : tab;
	}

	public static String infoMessage(AbstractCommand acmd) {
		StringBuilder argTrue = new StringBuilder();
		StringBuilder argFalse = new StringBuilder();
		if (acmd.getArgs() != null) {
			if (acmd.getArgs().containsKey(true)) {
				acmd.getArgs().get(true).forEach(x -> {
					argTrue.append(" <");
					argTrue.append((String) x);
					argTrue.append(">");
				});
			}
			if (acmd.getArgs().containsKey(false)) {
				acmd.getArgs().get(false).forEach(x -> {
					argTrue.append(" [");
					argTrue.append((String) x);
					argTrue.append("]");
				});
			}
		}
		return (Object) ChatColor.DARK_AQUA + "/" + cmd.getName() + " " + acmd.getCommand() + argTrue + argFalse
				+ (Object) ChatColor.GOLD + " - " + acmd.getDescription();
	}

	public static void setupCmds(DiabloLike plugin) {
		new ReloadCommand(plugin);
		new IdentifyCommand(plugin);
		new DropCommand(plugin);
		new CustomCommand(plugin);
		new TestCommand(plugin);
		new OrbsCommand(plugin);
		new ItemInfoCommand(plugin);
		new KolekceCommand(plugin);
		new RaritySetCommand(plugin);
		new ReidentifyCommand(plugin);
		new InventoryCommand(plugin);
		new ColorCommand(plugin);
		new AddCommand(plugin);
		new GUICommand(plugin);
		new LogCommand(plugin);
		new DebugCommand(plugin);
		cmd.setExecutor((CommandExecutor) new CommandHandler());
		AbstractCommand.CMDS.sort((x, y) -> x.getCommand().compareTo(y.getCommand()));
	}
}
