/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.identifying.Identify;
import git.doomshade.diablolike.utils.ItemUtils;

public class IdentifyCommand extends AbstractCommand {
	public IdentifyCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		ItemStack mh = ((Player) sender).getInventory().getItemInMainHand();
		if (!new ItemUtils(mh).isValid()) {
			return false;
		}
		Identify id = new Identify(mh);
		if (id.isIdentified()) {
			id.setIdentified(false);
			sender.sendMessage(Messages.ITEM_DEINDETIFIED.getMessage(mh.getItemMeta().getDisplayName()));
		} else {
			id.setIdentified(true);
			sender.sendMessage(Messages.IDENTIFIED.getMessage(mh.getItemMeta().getDisplayName()));
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		return null;
	}

	@Override
	public String getCommand() {
		return "identify";
	}

	@Override
	protected String getDescription() {
		return "identifikuje predmet v ruce";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
