package git.doomshade.diablolike.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import git.doomshade.diablolike.DiabloLike;

public class DebugCommand extends AbstractCommand {
	public static boolean DEBUG = false;

	public DebugCommand(DiabloLike plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		DEBUG = !DEBUG;
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCommand() {
		// TODO Auto-generated method stub
		return "debug";
	}

	@Override
	protected String getDescription() {
		// TODO Auto-generated method stub
		return "starts a debug";
	}

	@Override
	protected boolean requiresPlayer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean requiresOp() {
		// TODO Auto-generated method stub
		return true;
	}

}
