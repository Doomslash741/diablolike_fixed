/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 */
package git.doomshade.diablolike.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.gui.GUIApi;
import git.doomshade.diablolike.gui.GUIMain;

public class GUICommand extends AbstractCommand {
	public GUICommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player hrac = (Player) sender;
		hrac.openInventory(GUIMain.MAINGUI.getInventory());
		GUIApi.CURRENT_PAGE.put(hrac.getUniqueId(), 0);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		return null;
	}

	@Override
	public String getCommand() {
		return "gui";
	}

	@Override
	protected String getDescription() {
		return "otevre gui";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
