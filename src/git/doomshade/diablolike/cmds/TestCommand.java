/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 */
package git.doomshade.diablolike.cmds;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.cmds.AbstractCommand;
import java.util.List;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class TestCommand
extends AbstractCommand {
    public TestCommand(DiabloLike plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    protected Map<Boolean, List<String>> getArgs() {
        return null;
    }

    @Override
    public String getCommand() {
        return "test";
    }

    @Override
    protected String getDescription() {
        return "test cmd";
    }

    @Override
    protected boolean requiresPlayer() {
        return false;
    }

    @Override
    public boolean requiresOp() {
        return true;
    }
}

