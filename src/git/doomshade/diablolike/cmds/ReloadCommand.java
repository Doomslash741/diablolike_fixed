/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 */
package git.doomshade.diablolike.cmds;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.cmds.AbstractCommand;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.utils.Initializer;
import java.util.List;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ReloadCommand
extends AbstractCommand {
    public ReloadCommand(DiabloLike plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        this.plugin.reloadConfig();
        Initializer.initFiles();
        MainConfig.loadFiles();
        sender.sendMessage(Messages.PLUGIN_RELOADED.getMessage());
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    protected Map<Boolean, List<String>> getArgs() {
        return null;
    }

    @Override
    public String getCommand() {
        return "reload";
    }

    @Override
    protected String getDescription() {
        return "reloadne plugin";
    }

    @Override
    protected boolean requiresPlayer() {
        return false;
    }

    @Override
    public boolean requiresOp() {
        return true;
    }
}

