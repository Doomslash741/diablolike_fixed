/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package git.doomshade.diablolike.cmds;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.drops.Drop;
import git.doomshade.diablolike.drops.Range;

public class DropCommand extends AbstractCommand {
	public DropCommand(DiabloLike plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Range lvl;
		Player hrac = ((Player) sender);
		Player to = hrac;
		double chance = 100d;
		int amount = 1;

		if (args.length == 1) {
			sender.sendMessage(CommandHandler.infoMessage(this));
			return true;
		}
		lvl = Range.getFromString(args[1]);
		if (args.length >= 3) {
			to = Bukkit.getPlayer(args[2]);
		}
		if (args.length >= 4) {
			chance = Double.parseDouble(args[3]);
		}
		if (args.length >= 5) {
			amount = Integer.parseInt(args[4]);
		}
		// 5.-7. arg [tiercolor]
		Map<Chance, Double> addDrops = new HashMap<>();
		for (Chance ch : Chance.values()) {
			addDrops.put(ch, (double) ch.getChance());
		}
		addDrops.put(Chance.COMMON, (double) Chance.COMMON.getChance());
		switch (args.length) {
		case 8:
			addDrops.put(Chance.UNCOMMON, Double.parseDouble(args[7]));
		case 7:
			addDrops.put(Chance.RARE, Double.parseDouble(args[6]));
		case 6:
			addDrops.put(Chance.LEGENDARY, Double.parseDouble(args[5]));
			break;
		}

		Drop drop = new Drop(lvl);
		drop.setChance(chance);
		for (int i = 0; i < amount; ++i) {
			ItemStack item = drop.drop(addDrops);
			if (item == null)
				continue;
			to.getInventory().addItem(new ItemStack[] { item });
			if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
				to.sendMessage(Messages.ITEM_GIVEN.getMessage(item.getItemMeta().getDisplayName()));
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		HashMap<Boolean, List<String>> map = new HashMap<Boolean, List<String>>();
		map.put(true, Arrays.asList("level"));
		map.put(false, Arrays.asList("player", "chance", "amount", "legendary", "rare", "uncommon"));
		return map;
	}

	@Override
	public String getCommand() {
		return "drop";
	}

	@Override
	protected String getDescription() {
		return "dropne veci";
	}

	@Override
	protected boolean requiresPlayer() {
		return true;
	}

	@Override
	public boolean requiresOp() {
		return true;
	}
}
