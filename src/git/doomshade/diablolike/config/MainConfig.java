/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.Material
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.drops.sell.Attribute;
import git.doomshade.diablolike.drops.sell.Formula;
import git.doomshade.diablolike.drops.sell.Rarity;
import git.doomshade.diablolike.utils.ItemUtils;

public enum MainConfig {
	UNIDENTIFIED_ITEM_LORE, ORB_LORE;

	private static Map<MainConfig, Object> MAP;
	private static ConfigurationSection SECTION;
	private static List<String> prefix;
	private static List<String> suffix;
	private static List<String> gems;
	private static Map<String, Integer> bosses;
	public static int POCET_VYLEPSENI;
	public static int MAX_VYLEPSENI;
	private static FileConfiguration config;
	private static List<String> lore;
	private static Formula formula;
	private static List<Attribute> attributes;
	public static double PRICECAP;
	public static String PRICE_LORE;
	public static String VYLEPSENI_LORE;
	private static List<String> previousLore;
	public static final String CURRENT_VYLEPSENI_IN_LORE = "%curr";
	public static final String MAX_VYLEPSENI_IN_LORE = "%max";
	public static final String MONEY_IN_LORE = "%money";
	public static String VENDOR_INVENTORY_NAME;
	public static int VENDOR_MONEY_POSITION;
	public static int VENDOR_ACCEPT_BUTTION_POSITION;
	public static int VENDOR_DECLINE_BUTTON_POSITION;
	public static int VENDOR_INFO_POSITION;
	public static int MAX_MOB_LEVEL;
	public static int LEVEL_RANGE_MIN;
	public static int LEVEL_RANGE_MAX;
	private static ItemStack vendorMoneyItem;
	private static ItemStack vendorAcceptButtonItem;
	private static ItemStack vendorDeclineButtonItem;
	private static ItemStack vendorInfoItem;
	public static final int INVENTORY_SIZE = 54;
	private static List<String> excludedDungeons;
	private static List<String> excludedGemDisplayNames = new ArrayList<>();
	private static List<Material> excludedGemMaterials;
	private static Map<Chance, List<String>> customRarities;
	public static int OBRANA;
	public static Pattern DISPLAY_NAME_PATTERN;
	public static String LEVEL_RANGE_PATTERN;
	public static String RAW_VYLEPSENI_LORE;

	static {
		MAX_VYLEPSENI = 0;
		POCET_VYLEPSENI = 0;
		PRICECAP = 0;
		PRICE_LORE = "";
		VYLEPSENI_LORE = "";
		VENDOR_INVENTORY_NAME = "";
		VENDOR_MONEY_POSITION = 0;
		VENDOR_ACCEPT_BUTTION_POSITION = 0;
		VENDOR_DECLINE_BUTTON_POSITION = 0;
		VENDOR_INFO_POSITION = 0;
		MAX_MOB_LEVEL = 0;
		LEVEL_RANGE_MIN = 0;
		LEVEL_RANGE_MAX = 0;
		OBRANA = 65;
		RAW_VYLEPSENI_LORE = "";
	}

	public enum ArmorType {
		LEATHER, GOLD, CHAINMAIL, IRON, DIAMOND;
	}

	public static double getObranaMultiplier(ArmorType at) {
		if (!config.isConfigurationSection("obrana-multiplier")) {
			System.out.println("Neexistuje config sekce obrana-multiplier");
			return 1d;
		}
		return config.getConfigurationSection("obrana-multiplier").getDouble(at.name().toLowerCase(), 1d);
	}

	public static List<Attribute> getAttributes() {
		return attributes;
	}

	public static Map<String, Integer> getBossLevels() {
		return bosses;
	}

	public static Formula getFormula() {
		return formula;
	}

	public static List<String> getGems() {
		return gems;
	}

	public static List<String> getPrefixes() {
		return prefix;
	}

	public static List<String> getSuffixes() {
		return suffix;
	}

	public static List<String> getPreviousLore() {
		return previousLore;
	}

	public static ItemStack getVendorMoneyItem() {
		return vendorMoneyItem;
	}

	public static ItemStack getVendorAcceptButtonItem() {
		return vendorAcceptButtonItem;
	}

	public static ItemStack getVendorDeclineButtonItem() {
		return vendorDeclineButtonItem;
	}

	public static ItemStack getVendorInfoItem() {
		return vendorInfoItem;
	}

	public static List<String> getExcludedDungeons() {
		return excludedDungeons;
	}

	public static List<String> getExcludedGemDisplayNames() {
		return excludedGemDisplayNames;
	}

	public static List<Material> getExcludedGemMaterials() {
		return excludedGemMaterials;
	}

	public static Map<Chance, List<String>> getCustomRarities() {
		return customRarities;
	}

	public static void loadFiles() {
		MAP = new HashMap<MainConfig, Object>();
		config = DiabloLike.getInstance().getConfig();
		if (config.isConfigurationSection("item"))
			SECTION = config.getConfigurationSection("item");
		prefix = new ArrayList<String>();
		suffix = new ArrayList<String>();
		gems = new ArrayList<String>();
		bosses = new HashMap<String, Integer>();
		attributes = new ArrayList<Attribute>();
		previousLore = new ArrayList<String>();
		excludedDungeons = new ArrayList<String>();
		excludedGemDisplayNames = new ArrayList<String>();
		excludedGemMaterials = new ArrayList<Material>();
		MAX_MOB_LEVEL = config.getInt("mob-level");
		LEVEL_RANGE_MIN = config.getInt("range.min-level");
		LEVEL_RANGE_MAX = config.getInt("range.max-level");
		OBRANA = config.getInt("obrana");
		customRarities = new HashMap<Chance, List<String>>();
		MainConfig.orbFiles();
		MainConfig.socketGemFiles();
		MainConfig.bossFiles();
		MainConfig.priceFiles();
		MainConfig.vendorFiles();
		MainConfig.dungeonFiles();
		MainConfig.excludedGemFiles();
		MainConfig.customRarities();
		MainConfig.setupIdentifyPatterns();
	}

	private static void setupIdentifyPatterns() {
		if (SECTION == null) {
			return;
		}
		DISPLAY_NAME_PATTERN = Pattern.compile("[" + SECTION.getString("displayName") + "]");
		LEVEL_RANGE_PATTERN = SECTION.getString("lore");
	}

	private static void customRarities() {
		if (!config.isConfigurationSection("custom-rarities")) {
			System.out.println("Chyb� config sekce custom-rarities");
			return;
		}
		ConfigurationSection section = config.getConfigurationSection("custom-rarities");
		section.getKeys(false).forEach(x -> {
			Chance c = Chance.fromTierColor(TierColor.fromName(x));
			if (c != Chance.COMMON) {
				customRarities.put(c, section.getStringList(x));
			}
		});
	}

	private static void excludedGemFiles() {
		if (!config.isConfigurationSection("gem-exclusions")) {
			System.out.println("Chyb� config sekce gem-exclusions");
			return;
		}
		ConfigurationSection section = config.getConfigurationSection("gem-exclusions");
		excludedGemDisplayNames = section.getStringList("displayNames");
		config.getStringList("materials").forEach(x -> excludedGemMaterials.add(Material.getMaterial((String) x)));
	}

	private static void bossFiles() {
		File[] bossesFile = new File("plugins/MythicMobs/Mobs").listFiles();
		if (!bossesFile[0].exists()) {
			Bukkit.getLogger().log(Level.WARNING, "\u00a7cCesta k MM mobkam neexistuje!");
			return;
		}
		for (File file : bossesFile) {
			YamlConfiguration cf = YamlConfiguration.loadConfiguration((File) file);
			for (String s : cf.getKeys(false)) {
				ConfigurationSection sss = cf.getConfigurationSection(s);
				if (!sss.isString("Display"))
					continue;
				bosses.put(ChatColor.translateAlternateColorCodes((char) '&', (String) sss.getString("Display")),
						sss.isInt("lvl") ? sss.getInt("lvl") : Integer.MIN_VALUE);
			}
		}
	}

	private static void orbFiles() {
		if (!config.isConfigurationSection("orbs")) {
			System.out.println("Chyb� config sekce orbs");
			return;
		}
		ConfigurationSection orb = config.getConfigurationSection("orbs");
		MAX_VYLEPSENI = orb.getInt("orb-vylepseni.maximum-vylepseni");
		lore = config.getStringList("orb-lore");
		VYLEPSENI_LORE = ChatColor.translateAlternateColorCodes((char) '&',
				(String) orb.getString("orb-vylepseni.vylepseni-lore"));
		POCET_VYLEPSENI = orb.getInt("orb-vylepseni.pocet-vylepseni");
		RAW_VYLEPSENI_LORE = ChatColor.translateAlternateColorCodes((char) '&',
				(String) VYLEPSENI_LORE.replaceAll(CURRENT_VYLEPSENI_IN_LORE, String.valueOf(0))
						.replaceAll(MAX_VYLEPSENI_IN_LORE, String.valueOf(0)));
		MAP.put(UNIDENTIFIED_ITEM_LORE, ChatColor.translateAlternateColorCodes((char) '&',
				(String) config.getString("unidentified-item-lore")));
		MAP.put(ORB_LORE, MainConfig.orbLore());
	}

	private static List<String> orbLore() {
		ArrayList<String> finalLore = new ArrayList<String>();
		for (String s : lore) {
			finalLore.add(ChatColor.translateAlternateColorCodes((char) '&', (String) s));
		}
		return finalLore;
	}

	private static void priceFiles() {
		if (!config.isConfigurationSection("price-configuration")) {
			System.out.println("Chyb� config sekce price-configuration");
			return;
		}
		ConfigurationSection priceSection = config.getConfigurationSection("price-configuration");
		PRICECAP = priceSection.getDouble("cap");
		ConfigurationSection attributeSection = priceSection.getConfigurationSection("attributes");
		ConfigurationSection raritySection = priceSection.getConfigurationSection("rarity");
		for (String key : attributeSection.getKeys(false)) {
			attributes.add(new Attribute(Attribute.DefaultAttributes.fromName(key), attributeSection.getDouble(key)));
		}
		ArrayList<Rarity> rarities = new ArrayList<Rarity>();
		for (String key : raritySection.getKeys(false)) {
			rarities.add(new Rarity(TierColor.fromName(key), raritySection.getDouble(key)));
		}
		formula = new Formula(attributes, priceSection.getDouble("per-level"), rarities);
		PRICE_LORE = priceSection.getString("item-lore");
		previousLore = priceSection.getStringList("previous-lore");
	}

	private static void socketGemFiles() {
		File file = new File("plugins/MythicDrops/socketGems.yml");
		if (!file.isFile()) {
			Bukkit.getConsoleSender()
					.sendMessage((Object) ChatColor.RED + "Cesta ke gemum neexistuje! (" + file.getPath() + ")");
		} else {
			ConfigurationSection section = YamlConfiguration.loadConfiguration((File) file)
					.getConfigurationSection("socket-gems");
			for (String s : section.getKeys(false)) {
				gems.add(s);
				ConfigurationSection path = section.getConfigurationSection(s);
				if (path.isString("prefix")) {
					prefix.add(path.getString("prefix"));
				}
				if (!path.isString("suffix"))
					continue;
				suffix.add(path.getString("suffix"));
			}
		}
	}

	private static void vendorFiles() {
		if (!config.isConfigurationSection("vendor")) {
			System.out.println("Chyb� config sekce vendor");
			return;
		}
		ConfigurationSection vendorSection = config.getConfigurationSection("vendor");
		ConfigurationSection moneySection = vendorSection.getConfigurationSection("money-info");
		ConfigurationSection acceptSection = vendorSection.getConfigurationSection("accept-button");
		ConfigurationSection declineSection = vendorSection.getConfigurationSection("decline-button");
		ConfigurationSection infoSection = vendorSection.getConfigurationSection("info");
		VENDOR_INVENTORY_NAME = vendorSection.getString("inventory-name");
		vendorMoneyItem = MainConfig.getItemFromSection(moneySection);
		VENDOR_MONEY_POSITION = moneySection.getInt("position") - 1;
		vendorAcceptButtonItem = MainConfig.getItemFromSection(acceptSection);
		VENDOR_ACCEPT_BUTTION_POSITION = acceptSection.getInt("position") - 1;
		vendorDeclineButtonItem = MainConfig.getItemFromSection(declineSection);
		VENDOR_DECLINE_BUTTON_POSITION = declineSection.getInt("position") - 1;
		vendorInfoItem = MainConfig.getItemFromSection(infoSection);
		VENDOR_INFO_POSITION = infoSection.getInt("position") - 1;
	}

	private static ItemStack getItemFromSection(ConfigurationSection section) {
		return ItemUtils.makeItem(Material.getMaterial((String) section.getString("material")),
				section.getString("name"), section.getStringList("lore"), (short) section.getInt("data"),
				Color.fromRGB((int) section.getInt("x"), (int) section.getInt("y"), (int) section.getInt("z")));
	}

	public Object get() {
		return MAP.get((Object) this);
	}

	private static void dungeonFiles() {
		excludedDungeons = config.getStringList("excluded-dungeons");
	}
}
