/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.config;

import git.doomshade.diablolike.DiabloLike;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum TierColor {
	COMMON, UNCOMMON, RARE, LEGENDARY, MYTHIC;

	private static Map<TierColor, String> MAP;

	static {
		MAP = new HashMap<TierColor, String>();
		FileConfiguration config = DiabloLike.getInstance().getConfig();
		ConfigurationSection section = config.getConfigurationSection("tier-colors");
		for (TierColor tc : values()) {
			MAP.put(tc, ChatColor.translateAlternateColorCodes((char) '&', (String) section.getString(tc.toString())));
		}
	}

	public static TierColor fromName(String name) {
		for (TierColor tc : TierColor.values()) {
			if (tc.toString().equalsIgnoreCase(name))
				return tc;
		}
		return null;
	}

	public String getColor() {
		return MAP.get((Object) this);
	}

	public ItemStack setRarity(ItemStack item) {
		ItemStack itemm = item.clone();
		ItemMeta meta = itemm.getItemMeta();
		String name = String.valueOf(this.getColor())
				+ meta.getDisplayName().substring(2, meta.getDisplayName().length());
		meta.setDisplayName(name);
		itemm.setItemMeta(meta);
		return itemm;
	}

	@Override
	public String toString() {
		return this.name().toLowerCase();
	}
}
