/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package git.doomshade.diablolike.config;

import git.doomshade.diablolike.DiabloLike;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Messages {
    PREFIX("prefix-identify"),
    IDENTIFIED("identified"),
    NOT_IDENTIFIABLE("item-not-identifiable"),
    NOW_IDENTIFIYING("now-identifying"),
    TOO_MANY_ITEMS("too-many-items"),
    NOW_GEMMING("now-using-gem"),
    GEMMED("successful-gem"),
    MAX_VYLEPSENI("maximum-enchantments"),
    TOO_MANY_ITEMS_GEM("too-many-items-gem"),
    PLUGIN_RELOADED("plugin-reloaded"),
    ITEM_GIVEN("given-item"),
    CANNOT_GIVE_INFO("cannot-give-info-about-item"),
    NO_ITEM_IN_HAND("no-item-in-hand"),
    ITEM_DEINDETIFIED("item-deidentified");
    
    private String s;
    private static Map<Messages, String> MESSAGES;

    private Messages(String s) {
        this.s = s;
    }

    @Override
	public String toString() {
        return this.s;
    }

    public static Map<String, Object> defaultMessages() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(PREFIX.toString(), "[Identifikace]");
        map.put(IDENTIFIED.toString(), "&aPredmet &3%item &aidentifikovan!");
        map.put(NOT_IDENTIFIABLE.toString(), "&3%item&c nelze identifikovat!");
        map.put(NOW_IDENTIFIYING.toString(), "&aNyni klikni na &c&oNeidentifikovany predmet &a!");
        map.put(TOO_MANY_ITEMS.toString(), "&cMuzes identifikovat pouze jeden predmet!");
        map.put(NOW_GEMMING.toString(), "&aNyni klikni na predmet, na ktery chces tento orb pouzit!");
        map.put(GEMMED.toString(), "&aUspesne jsi pouzil orb!");
        map.put(MAX_VYLEPSENI.toString(), "&3%item&c je jiz vylepseni na maximum!");
        map.put(TOO_MANY_ITEMS_GEM.toString(), "&cMuzes vlozit orb pouze do jednoho predmetu!");
        map.put(PLUGIN_RELOADED.toString(), "&aPlugin reloadnut.");
        map.put(ITEM_GIVEN.toString(), "&aZiskal jsi &3%item&a.");
        map.put(CANNOT_GIVE_INFO.toString(), "&cNelze ziskat blizsi informace o &3%item&c.");
        map.put(NO_ITEM_IN_HAND.toString(), "&cNemas zadny predmet v ruce.");
        map.put(ITEM_DEINDETIFIED.toString(), "&aPredmet &3%item &adeidentifikovan!");
        return map;
    }

    private static String convert(Messages m) {
        YamlConfiguration loader = YamlConfiguration.loadConfiguration((File)DiabloLike.getInstance().getMessages());
        return loader.getString(m.toString()) != "" ? String.valueOf(ChatColor.translateAlternateColorCodes((char)'&', (String)loader.getString(m.toString()))) + " " : "";
    }

    public static void writeMessages() {
        MESSAGES = new HashMap<Messages, String>();
        MESSAGES.put(PREFIX, Messages.convert(PREFIX));
        MESSAGES.put(IDENTIFIED, String.valueOf(Messages.convert(PREFIX)) + Messages.convert(IDENTIFIED));
        MESSAGES.put(NOT_IDENTIFIABLE, String.valueOf(Messages.convert(PREFIX)) + Messages.convert(NOT_IDENTIFIABLE));
        MESSAGES.put(NOW_IDENTIFIYING, String.valueOf(Messages.convert(PREFIX)) + Messages.convert(NOW_IDENTIFIYING));
        MESSAGES.put(TOO_MANY_ITEMS, String.valueOf(Messages.convert(PREFIX)) + Messages.convert(TOO_MANY_ITEMS));
        MESSAGES.put(NOW_GEMMING, Messages.convert(NOW_GEMMING));
        MESSAGES.put(GEMMED, Messages.convert(GEMMED));
        MESSAGES.put(MAX_VYLEPSENI, Messages.convert(MAX_VYLEPSENI));
        MESSAGES.put(TOO_MANY_ITEMS_GEM, Messages.convert(TOO_MANY_ITEMS_GEM));
        MESSAGES.put(PLUGIN_RELOADED, Messages.convert(PLUGIN_RELOADED));
        MESSAGES.put(ITEM_GIVEN, Messages.convert(ITEM_GIVEN));
        MESSAGES.put(CANNOT_GIVE_INFO, Messages.convert(CANNOT_GIVE_INFO));
        MESSAGES.put(NO_ITEM_IN_HAND, Messages.convert(NO_ITEM_IN_HAND));
        MESSAGES.put(ITEM_DEINDETIFIED, String.valueOf(Messages.convert(PREFIX)) + Messages.convert(ITEM_DEINDETIFIED));
    }

    public String getMessage() {
        return MESSAGES.get((Object)this);
    }

    public String getMessage(String itemName) {
        return MESSAGES.get((Object)this).replaceAll("%item", itemName);
    }
}

