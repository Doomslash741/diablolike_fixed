/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 */
package git.doomshade.diablolike.config;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import git.doomshade.diablolike.DiabloLike;

public enum Chance {
    COMMON,
    UNCOMMON,
    RARE,
    LEGENDARY,
    MYTHIC,
    DROP;
    
    private static Map<Chance, Integer> map;
    private static Map<Chance, Integer> gems;

    static {
        map = new HashMap<Chance, Integer>();
        gems = new HashMap<Chance, Integer>();
        Chance.loadFiles();
    }

    public static Map<Chance, Integer> gemChances() {
        return gems;
    }

    public static void loadFiles() {
        try {
            Chance.map();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Chance.gems();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void gems() {
        ConfigurationSection gemSec = DiabloLike.getInstance().getConfig().getConfigurationSection("gem-chance");
        for (Chance chance : Chance.values()) {
            gems.put(chance, gemSec.getInt(chance.toString()));
        }
    }

    private static void map() {
        ConfigurationSection section = DiabloLike.getInstance().getConfig().getConfigurationSection("chance");
        for (Chance chance : Chance.values()) {
            map.put(chance, section.getInt(chance.toString()));
        }
        map.put(DROP, DiabloLike.getInstance().getConfig().getInt("drop-chance"));
    }

    @Override
	public String toString() {
        return this.name().equalsIgnoreCase("drop") ? "chance" : this.name().toLowerCase();
    }

    public static Chance fromTierColor(TierColor tc) {
        for (Chance c : Chance.values()) {
            if (!tc.toString().equalsIgnoreCase(c.toString())) continue;
            return c;
        }
        Bukkit.getConsoleSender().sendMessage("Returned common chance as default (this should not happen) (Chance)");
        return COMMON;
    }

    public static boolean isChance(String chance) {
        for (Chance c : Chance.values()) {
            if (!c.toString().equalsIgnoreCase(chance)) continue;
            return true;
        }
        return false;
    }

    public static Chance fromString(String s) {
        for (Chance c : Chance.values()) {
            if (!s.equalsIgnoreCase(c.toString())) continue;
            return c;
        }
        return null;
    }

    public int getChance() {
        return map.get((Object)this);
    }
}

