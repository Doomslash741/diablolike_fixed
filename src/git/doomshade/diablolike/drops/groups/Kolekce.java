/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.drops.groups;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.drops.Drop;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.ItemUtils;

public class Kolekce implements Iterable<DiabloItem> {
	private String name;
	private Range amount;
	private double chance;
	private File kolekceFile;
	private FileConfiguration loader;
	private List<DiabloItem> kolekce;
	private static final Random random = new Random();
	private Map<Chance, Double> additionalChance;

	private static File getFile(String name) {
		DiabloLike plugin = DiabloLike.getInstance();
		File file = new File(plugin.getCOLLECTIONS_FOLDER() + File.separator + name + ".yml");
		if (file.exists()) {
			return file;
		}
		file = new File(plugin.getSpecificLootFolder() + File.separator + name + ".yml");
		if (file.exists()) {
			return file;
		}
		return null;
	}

	public Kolekce(String file) {
		this(Kolekce.getFile(Kolekce.getNameFromFile(file)));
	}

	public Kolekce(File file) {
		if (file == null) {
			return;
		}
		this.kolekceFile = file;
		this.loader = YamlConfiguration.loadConfiguration((File) this.kolekceFile);
		this.name = file.getName().substring(0, file.getName().length() - 4);
		this.amount = new Range(1);
		this.chance = Chance.DROP.getChance();
		this.loadKolekce();
	}

	public Kolekce(Kolekce collection) {
		this(collection.getFile());
	}

	public void setRarityChance(Chance type, double chance) {
		this.additionalChance.put(type, chance);
	}

	public double getRarityChance(Chance type) {
		if (this.additionalChance == null || !this.additionalChance.containsKey((Object) type)) {
			return type.getChance();
		}
		return this.additionalChance.get((Object) type);
	}

	public Map<Chance, Double> getAdditionalChances() {
		return this.additionalChance;
	}

	public File getFile() {
		return this.kolekceFile;
	}

	public void loadKolekce() {
		this.additionalChance = new HashMap<Chance, Double>();
		for (Chance type : Chance.values()) {
			if (!this.loader.contains(type.toString()))
				continue;
			this.additionalChance.put(type, this.loader.getDouble(type.toString()));
		}
		this.kolekce = new ArrayList<DiabloItem>();
		if (!this.exists()) {
			return;
		}
		
		for (String key : loader.getKeys(false)) {
			switch(key) {
			case "amount":
				this.amount = Range.getFromString(loader.getString(key));
				break;
			case "chance":
				this.chance = loader.getDouble(key);
				break;
			case "kolekce":
				List<String> kolekceNames = new ArrayList<>(this.loader.getStringList(key));
				for (String s : kolekceNames) {
					this.kolekce.addAll(DiabloLike.getCollection(s).kolekce);
				}
				if (!Chance.isChance(key)) {
					this.kolekce.add(DiabloLike.getItemFromConfigName(key));
				}
				break;
			case "level":
				List<DiabloItem> its = new ArrayList<>();
				if (this.loader.isString(key)) {
					its = new Drop(Range.getFromString(this.loader.getString(key))).availableItems();
				} else if (this.loader.isInt(key)) {
					its = new Drop(this.loader.getInt(key)).availableItems();
				}
				this.kolekce.addAll(its);
				break;
			default:
				DiabloItem item = ItemUtils.getDiabloItemFromFile(key);
				if (item != null) {
					this.kolekce.add(item);
				}
				break;
			}
		}
	}

	public List<DiabloItem> getKolekce() {
		return this.kolekce;
	}

	public List<ItemStack> getRandomDrop() {
		ArrayList<ItemStack> drop = new ArrayList<ItemStack>();
		if (!this.canDrop()) {
			return drop;
		}
		int randomAmount = this.amount.getRandom();
		if (randomAmount == this.kolekce.size()) {
			for (int i = 0; i < this.kolekce.size(); ++i) {
				drop.set(i, new ItemUtils(this.kolekce.get(i)).getDropItem(this));
			}
			return drop;
		}
		ArrayList<Integer> alreadyInDrop = new ArrayList<Integer>();
		int maxAmount = 30;
		for (int i = 0; i < randomAmount; ++i) {
			int randomDrop = random.nextInt(this.kolekce.size());
			while (alreadyInDrop.contains(randomDrop) && maxAmount != 0) {
				randomDrop = random.nextInt(this.kolekce.size());
				--maxAmount;
			}
			if (this.kolekce.get(randomDrop) == null)
				continue;
			alreadyInDrop.add(randomDrop);
			drop.add(new ItemUtils(this.kolekce.get(randomDrop)).getDropItem(this));
		}
		return drop;
	}

	public DiabloItem getRandomItem() {
		return this.kolekce.get(new Random().nextInt(this.kolekce.size()));
	}

	public ItemStack getRandomFinalItem() {
		return new ItemUtils(this.getRandomItem()).getDropItem(this);
	}

	@SuppressWarnings("unused")
	private DiabloItem getVazenyFinalItem() {
		if (!this.isVazena()) {
			return this.getRandomItem();
		}
		double randomVaha = Math.random() * 100.0;
		double vaha = 0.0;
		DiabloItem vazenyItem = null;
		for (DiabloItem item : this.getVahy().keySet()) {
			if (!((vaha += this.getVahy().get(item).doubleValue()) >= randomVaha))
				continue;
			vazenyItem = item;
			break;
		}
		return vazenyItem;
	}

	private Map<DiabloItem, Double> getVahy() {
		HashMap<DiabloItem, Double> map = new HashMap<DiabloItem, Double>();
		/*Iterator iterator = this.loader.getKeys(false).iterator();
		while (iterator.hasNext()) {
			ConfigurationSection itemSection;
			String s;
			map.put(DiabloLike.getItemFromConfigName(s),
					(itemSection = this.loader.getConfigurationSection(s = (String) iterator.next())).isDouble("vaha")
							? itemSection.getDouble("vaha")
							: 0.0);
		}*/
		return map;
	}

	@SuppressWarnings("unused")
	private double getVahyAmount() {
		double vaha = 0.0;
		Iterator<Double> it = this.getVahy().values().iterator();
		while (it.hasNext()) {
			vaha += it.next().doubleValue();
		}
		if (vaha != 100.0) {
			Bukkit.getConsoleSender().sendMessage("Vahy u kolekce " + this.name + " se nerovnaji 100! (" + vaha + ")");
		}
		return vaha;
	}

	public String getName() {
		return this.name;
	}

	public boolean canDrop() {
		return new Random().nextDouble() * 100.0 < this.getChance();
	}

	public Range getAmount() {
		return this.amount;
	}

	public void setAmount(Range amount) {
		this.amount = amount;
	}

	public double getChance() {
		return this.chance;
	}

	public void setChance(double chance) {
		this.chance = chance;
	}

	public boolean exists() {
		return this.kolekceFile == null ? false : this.kolekceFile.exists();
	}

	public boolean isVazena() {
		return this.loader.isBoolean("vazena") ? this.loader.getBoolean("vazena") : false;
	}

	public boolean isSimilar(Kolekce kolekce) {
		return Kolekce.isSimilar(this, kolekce);
	}

	public static boolean isSimilar(Kolekce one, Kolekce two) {
		return one.getName().equalsIgnoreCase(two.getName());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder().append("\nname: " + this.name + "\n")
				.append("chance: " + this.chance + "\n").append("amount: " + this.amount + "\n");
		for (Chance c : Chance.values()) {
			sb.append(String.valueOf(c.toString()) + ": " + this.getRarityChance(c) + "\n");
		}
		return sb.toString();
	}

	public static String getNameFromFile(File file) {
		return Kolekce.getNameFromFile(file.getName());
	}

	public static String getNameFromFile(String file) {
		if (!file.contains(".yml")) {
			return file;
		}
		return file.substring(0, file.length() - 4);
	}

	@Override
	public Iterator<DiabloItem> iterator() {
		// TODO Auto-generated method stub
		return kolekce.iterator();
	}
}
