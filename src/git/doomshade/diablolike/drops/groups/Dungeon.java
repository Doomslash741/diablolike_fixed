/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package git.doomshade.diablolike.drops.groups;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.drops.Range;
import git.doomshade.diablolike.utils.Utils;

public class Dungeon
implements FilenameFilter {
    private String name;
    private File file;
    private static Kolekce TEMPLATE_COLLECTION;
    private Map<String, List<Kolekce>> mobDrop;

    public Dungeon(String name) {
        this(new File(DiabloLike.getInstance().getDUNGEONS_FOLDER() + File.separator + name + ".yml"));
    }

    private Dungeon(File file) {
        this.name = file.getName().substring(0, file.getName().length() - 4);
        this.file = file;
        if (!this.exists()) {
            if (!this.name.isEmpty()) {
                Bukkit.getConsoleSender().sendMessage("(Dungeon) Dungeon " + this.name + " neexistuje.");
            }
            return;
        }
        this.initDrop();
    }

    public boolean isSimilar(Dungeon two) {
        return Dungeon.isSimilar(this, two);
    }

    public static boolean isSimilar(Dungeon one, Dungeon two) {
        return one.getName().equalsIgnoreCase(two.getName());
    }

    public String getName() {
        return this.name;
    }

    public void initDrop() {
        this.mobDrop = new HashMap<String, List<Kolekce>>();
        YamlConfiguration loader = YamlConfiguration.loadConfiguration((File)this.file);
        for (String s : loader.getKeys(false)) {
            ArrayList<Kolekce> listKolekce = new ArrayList<Kolekce>();
            ConfigurationSection dungeonMob = loader.getConfigurationSection(s);
            for (String kolekceName : dungeonMob.getKeys(false)) {
                if (!dungeonMob.isConfigurationSection(kolekceName)) continue;
                ConfigurationSection kolekce = dungeonMob.getConfigurationSection(kolekceName);
                Kolekce realKolekce = new Kolekce(DiabloLike.getCollection(kolekceName));
                if (!realKolekce.exists()) {
                	continue;
                }
                if (kolekce.isString("amount")) {
                    realKolekce.setAmount(Range.getFromString(kolekce.getString("amount")));
                } else {
                    realKolekce.setAmount(new Range(kolekce.getInt("amount")));
                }
                for (Chance chance : Chance.values()) {
                    if (!kolekce.contains(chance.toString())) continue;
                    realKolekce.setRarityChance(chance, kolekce.getDouble(chance.toString()));
                }
                realKolekce.setChance(kolekce.getDouble("chance"));
                listKolekce.add(realKolekce);
            }
            this.mobDrop.put(s, listKolekce);
            for (Kolekce kolekcee : this.mobDrop.get(s)) {
                if (kolekcee != null && kolekcee.getName() != null) continue;
                Bukkit.getConsoleSender().sendMessage("Chyba v kolekci " + this.file.getName() + " u mobky " + s);
            }
        }
    }

    private static void initFile(File file) throws IOException {
        File dungFile = new File(DiabloLike.getInstance().getDUNGEONS_FOLDER() + File.separator + file.getName());
        if (!dungFile.exists()) {
            dungFile.createNewFile();
        }
        YamlConfiguration loader = YamlConfiguration.loadConfiguration((File)dungFile);
        YamlConfiguration mobLoader = YamlConfiguration.loadConfiguration((File)file);
        boolean shouldSave = false;
        for (String s : mobLoader.getKeys(false)) {
            String fileMobName = mobLoader.getConfigurationSection(s).isString("Display") ? mobLoader.getConfigurationSection(s).getString("Display") : "";
            if (!fileMobName.isEmpty()) {
                Utils.DUNGEON_MOB_NAMES.put(ChatColor.stripColor((String)ChatColor.translateAlternateColorCodes((char)'&', (String)fileMobName)), s);
            }
            if (loader.isConfigurationSection(s)) continue;
            shouldSave = true;
            ConfigurationSection mobName = loader.createSection(s);
            ConfigurationSection collection = mobName.createSection(TEMPLATE_COLLECTION.getName());
            if (!collection.isSet("amount")) {
                collection.set("amount", (Object)TEMPLATE_COLLECTION.getAmount().toString());
            }
            if (collection.isSet("chance")) continue;
            collection.set("chance", (Object)TEMPLATE_COLLECTION.getChance());
        }
        if (shouldSave) {
            loader.save(dungFile);
        }
        Dungeon dung = new Dungeon(dungFile);
        Utils.DUNGEONS.put(dung.getName(), dung);
    }

    public static void initFiles() {
    	Bukkit.getConsoleSender().sendMessage(String.format("%sInitializing dungeons...", DiabloLike.getInstance().getPluginName()));
        Utils.DUNGEONS = new TreeMap<String, Dungeon>();
        File tempCollectionFile = new File(DiabloLike.getInstance().getCOLLECTIONS_FOLDER() + File.separator + "SinisterKolekce" + ".yml");
        if (!tempCollectionFile.exists()) {
        	try {
				tempCollectionFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        TEMPLATE_COLLECTION = new Kolekce("SinisterKolekce");
        for (File file : DiabloLike.getInstance().getMythicMobFiles()) {
            try {
                Dungeon.initFile(file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        Bukkit.getConsoleSender().sendMessage(String.valueOf(DiabloLike.getInstance().getPluginName()) + (Object)ChatColor.GREEN + "Loaded " + Utils.DUNGEONS.size() + " dungeons.");
    }

    public File getDungeonFile() {
        return this.file;
    }

    public boolean exists() {
        return this.file.exists();
    }

    public Map<String, List<Kolekce>> getDrops() {
        return this.mobDrop;
    }

    @Override
    public boolean accept(File dir, String fileName) {
        return !MainConfig.getExcludedDungeons().contains(fileName) && !MainConfig.getExcludedDungeons().contains(fileName.substring(0, fileName.length() - 4));
    }
}

