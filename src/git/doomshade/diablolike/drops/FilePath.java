/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.configuration.ConfigurationSection
 */
package git.doomshade.diablolike.drops;

import git.doomshade.diablolike.config.Chance;
import org.bukkit.configuration.ConfigurationSection;

public class FilePath {
    private ConfigurationSection section;
    public static final String ALWAYS_DROP = "always-drop";
    public static final String CHANCE = "chance";
    public static final String COMMON = "common";
    public static final String UNCOMMON = "uncommon";
    public static final String RARE = "rare";
    public static final String LEGENDARY = "legendary";
    public static final String AMOUNT_PER_LEVEL = "amount-pl";
    public static final String CHANCE_PER_LEVEL = "chance-pl";
    public static final String IDENTIFIED = "identified";
    public static final String CUSTOM_ITEM_SETTINGS = "custom-item-settings";
    public static final String ELITES = "elites";
    public static final String BOSSES = "bosses";
    public static final String LOOT_SECTION = "loot-options";
    public static final String EXTRA_LOOT_SECTION = "extra-loot";
    public static final String AMOUNT = "amount";
    public static final String COLLECTIONS = "collections";
    public static final String LORE = "lore";
    public static final String DISPLAY_NAME = "displayName";
    public static final String MATERIAL = "material";
    public static final String LEVEL = "level";
    public static final String KOLEKCE = "kolekce";

    public FilePath(ConfigurationSection section) {
        this.section = section;
    }

    public boolean alwaysDrop() {
        return this.section.isBoolean(ALWAYS_DROP) ? this.section.getBoolean(ALWAYS_DROP) : false;
    }

    public boolean isAlwaysIdentified() {
        return this.section.isBoolean(IDENTIFIED) ? this.section.getBoolean(IDENTIFIED) : false;
    }

    public boolean isConfigurationSection(String section) {
        return this.section.isConfigurationSection(section);
    }

    public double getDropChance() {
        return this.section.isDouble(CHANCE) ? this.section.getDouble(CHANCE) : (double)Chance.DROP.getChance();
    }

    public ConfigurationSection getConfigurationSection(String section) {
        return this.section.getConfigurationSection(section);
    }
}

