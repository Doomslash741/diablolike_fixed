/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.drops;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class Drop {
	public static final Random random = new Random();
	public static final Pattern pattern = Pattern.compile("\u00a72Pot�ebn� Lvl: [0-9]+");
	private List<DiabloItem> items;
	private final Range mobLevel;
	private double chance = (double) Chance.DROP.getChance();

	public Drop(int mobLevel) {
		this(new Range(mobLevel - MainConfig.LEVEL_RANGE_MIN, mobLevel + MainConfig.LEVEL_RANGE_MAX));
	}

	public Drop(Range levelRange) {
		this.mobLevel = levelRange;
		this.items = new ArrayList<DiabloItem>();
		for (int i = this.mobLevel.getMin(); i <= this.mobLevel.getMax(); i++) {
			if (!Utils.DROPS_PER_LEVEL.containsKey(i))
				continue;
			for (DiabloItem item : Utils.DROPS_PER_LEVEL.get(i)) {
				if (item.getDropChance() != 0) {
					items.add(item);

				}
			}
		}
	}

	public ItemStack drop() {
		if (!this.canDrop()) {
			return null;
		}
		int a = getRandomDrop();
		if (a == -1) {
			return null;
		}
		DiabloItem item = this.items.get(a);
		if (item == null || item.getItem() == null || !canBeDropped(item.getItem())) {
			return null;
		}
		this.items.remove(a);
		item.getItem().setAmount(item.getAmount().getRandom());
		ItemStack drop = new ItemUtils(item.getItem()).getDropItem();
		return drop;
	}

	public ItemStack drop(Map<Chance, Double> chances) {
		if (!this.canDrop()) {
			return null;
		}
		int a = getRandomDrop();
		if (a == -1) {
			return null;
		}
		DiabloItem item = this.items.get(a);
		if (item == null || item.getItem() == null || !canBeDropped(item.getItem())) {
			return null;
		}
		this.items.remove(a);
		item.getItem().setAmount(item.getAmount().getRandom());
		return new ItemUtils(item).getDropItemMap(chances);
	}

	private int getRandomDrop() {
		if (items.size() == 0) {
			return -1;
		}
		return random.nextInt(items.size());
	}

	public List<DiabloItem> availableItems() {
		return this.items;
	}

	private boolean canDrop() {
		return this.items.size() > 0;
	}

	// MISTAKE
	private boolean canBeDropped(ItemStack item) {
		return (Drop.random.nextDouble() * 100.0) <= chance;
	}

	public void setChance(double chance) {
		this.chance = chance;
	}
}
