/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 */
package git.doomshade.diablolike.drops;

import java.util.Random;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;

public class Range {
	private int min;
	private int max;
	private int num;
	private Range otherRange;

	public Range(int a, int b) {
		this.min = Math.min(a, b);
		this.max = Math.max(a, b);
		this.num = -1;
	}

	public Range(int a) {
		this(a, a);
	}

	public int getMax() {
		return this.max;
	}

	public int getMin() {
		return this.min;
	}

	public int getNum() {
		return this.num;
	}

	public Range getOtherRange() {
		return this.otherRange;
	}

	public boolean isInRange(int num) {
		return this.isInRange(num, true);
	}

	public boolean isInRange(int num, boolean includeInRange) {
		return includeInRange ? this.min <= num && this.max >= num : this.min < num && this.max > num;
	}

	public int getRandom() {
		return new Random().nextInt(1 + this.max - this.min) + this.min;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public void setRange(Range range) {
		this.otherRange = range;
	}

	public static Range getFromString(String range) {
		String regex = "[0-9]+[-][0-9]+";
		Pattern pattern = Pattern.compile(regex);
		String rangeCopy = range;
		if (pattern.matcher(rangeCopy = rangeCopy.trim()).find()) {
			String[] split = rangeCopy.split("-");
			return new Range(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
		}
		pattern = Pattern.compile("[0-9]+");
		if (pattern.matcher(rangeCopy).find()) {
			return new Range(Integer.parseInt(rangeCopy));
		}
		Bukkit.getConsoleSender().sendMessage(rangeCopy + "?");
		return null;
	}

	@Override
	public String toString() {
		return this.min == this.max ? String.valueOf(this.min) : String.valueOf(this.min) + "-" + this.max;
	}
}
