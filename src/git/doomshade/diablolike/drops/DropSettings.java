/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.drops;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.utils.ItemUtils;

public class DropSettings {
	private FilePath sec;
	private ItemStack item;
	public static Map<ItemStack, Double> drops = new HashMap<ItemStack, Double>();
	public static List<String> identified = new ArrayList<String>();
	public static List<String> alwaysDrops = new ArrayList<String>();
	public static Map<String, List<Range>> dropsPerLevel = new HashMap<String, List<Range>>();
	public static Map<String, List<Range>> chancePerLevel = new HashMap<String, List<Range>>();

	public DropSettings(String key, FileConfiguration loader) {
		this.sec = new FilePath(loader.getConfigurationSection(key));
		this.item = ItemUtils.getItemFromFile(key);
		if (this.item == null) {
			return;
		}
		this.allLevelDrops();
		this.drops();
		this.identify();
	}

	public static void loadAllDrops() {
		YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) DiabloLike.getInstance().getItems());
		for (String key : loader.getKeys(false)) {
			new git.doomshade.diablolike.drops.DropSettings(key, (FileConfiguration) loader);
		}
	}

	private void allLevelDrops() {
		if (!this.sec.alwaysDrop()) {
			return;
		}
		String itemName = ChatColor.stripColor((String) this.item.getItemMeta().getDisplayName());
		alwaysDrops.add(itemName);
		this.addDrops("amount-pl", dropsPerLevel);
		this.addDrops("chance-pl", chancePerLevel);
	}

	private void addDrops(String section, Map<String, List<Range>> map) {
		if (!this.sec.isConfigurationSection(section)) {
			return;
		}
		String itemName = ChatColor.stripColor((String) this.item.getItemMeta().getDisplayName());
		ConfigurationSection chance = this.sec.getConfigurationSection(section);
		ArrayList<Range> ranges = new ArrayList<Range>();
		for (String secKey : chance.getKeys(false)) {
			try {
				String[] split = secKey.split("-");
				int min = Integer.parseInt(split[0]);
				int max = Integer.parseInt(split[1]);
				Range range = new Range(min, max);
				if (chance.isInt(secKey)) {
					range.setNum(chance.getInt(secKey));
				} else {
					String[] rSplit = chance.getString(secKey).split("-");
					int rMin = Integer.parseInt(rSplit[0]);
					int rMax = Integer.parseInt(rSplit[1]);
					Range other = new Range(rMin, rMax);
					range.setRange(other);
				}
				ranges.add(range);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		map.put(itemName, ranges);
	}

	private void drops() {
		drops.put(this.item, this.sec.getDropChance());
	}

	private void identify() {
		if (this.sec.isAlwaysIdentified()) {
			identified.add(ChatColor.stripColor((String) this.item.getItemMeta().getDisplayName()));
		}
	}
}
