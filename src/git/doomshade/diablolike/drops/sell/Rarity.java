/*
 * Decompiled with CFR 0.139.
 */
package git.doomshade.diablolike.drops.sell;

import git.doomshade.diablolike.config.TierColor;

public class Rarity {
    private final TierColor tier;
    private final double amount;

    public Rarity(TierColor tier, double amount) {
        this.tier = tier;
        this.amount = amount;
    }

    public TierColor getTier() {
        return this.tier;
    }

    public double getPrice() {
        return this.amount;
    }
}

