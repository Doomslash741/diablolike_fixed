/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.drops.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class Attribute {
	private final DefaultAttributes name;
	private final double price;

	public Attribute(DefaultAttributes name, double price) {
		this.name = name;
		this.price = price;
	}

	public DefaultAttributes getAttribute() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	public static Map<DefaultAttributes, Integer> getAttributes(ItemStack item) {
		HashMap<DefaultAttributes, Integer> attrs = new HashMap<DefaultAttributes, Integer>();
		if (item != null && item.hasItemMeta() && item.getItemMeta().hasLore()) {
			List<String> lore = item.getItemMeta().getLore();
			for (int i = 0; i < lore.size(); ++i) {
				String loreLine = (String) lore.get(i);
				for (DefaultAttributes stat : DefaultAttributes.values()) {
					String statName = DefaultAttributes.getName(stat);
					if (!ChatColor.stripColor((String) loreLine).replaceAll("[-%+:0-9]+", "").trim()
							.equals(ChatColor.stripColor((String) statName)))
						continue;
					int pocetStatu = 0;
					try {
						pocetStatu = Integer
								.parseInt(ChatColor.stripColor((String) loreLine).replaceAll("[^0-9]+", ""));
						attrs.put(stat, pocetStatu);
					} catch (NumberFormatException numberFormatException) {
						// empty catch block
					}
				}
			}
		}
		return attrs;
	}

	public static enum DefaultAttributes {
		ARMOR("armor"), POSKOZENI("poskozeni"), VYHYBANI("vyhybani"), KRITICKY_ZASAH("sance-crit"), KRITICKY_UTOK(
				"crit"), ZIVOTY("zivoty"), VIRA("vira"), MANA_REGEN("mana-regen"), NAVYSENI_OBRANY(
						"reduc-dmg"), NAVYSENI_OBRANY_KOUZLA("reduc-spelldmg"), PRESNOST("presnost"), SILA(
								"sila"), OBRATNOST("obratnost"), INTELIGENCE("inteligence"), KLENOT(
										"klenot"), SILA_LECEBNYCH_KOUZEL("sila-lecebnych-kouzel");

		private String s;
		public static final Map<String, DefaultAttributes> NOCOLOR_NAME_MAP;
		private static final Map<String, DefaultAttributes> NAME_MAP;
		private static final Map<DefaultAttributes, String> NAMES;

		static {
			NOCOLOR_NAME_MAP = new HashMap<String, DefaultAttributes>();
			NAME_MAP = new HashMap<String, DefaultAttributes>();
			NAMES = new HashMap<DefaultAttributes, String>();
			NAME_MAP.put((Object) ChatColor.RED + "Po�kozen�", POSKOZENI);
			NAME_MAP.put((Object) ChatColor.DARK_GREEN + "Vyh�b�n�", VYHYBANI);
			NAME_MAP.put((Object) ChatColor.YELLOW + "Kritick� �tok", KRITICKY_UTOK);
			NAME_MAP.put((Object) ChatColor.GOLD + "�ance na kritick� z�sah", KRITICKY_ZASAH);
			NAME_MAP.put((Object) ChatColor.RED + "Vitalita", ZIVOTY);
			NAME_MAP.put((Object) ChatColor.DARK_AQUA + "V�ra", VIRA);
			NAME_MAP.put((Object) ChatColor.BLUE + "Inteligence", INTELIGENCE);
			NAME_MAP.put((Object) ChatColor.AQUA + "Regenerace many", MANA_REGEN);
			NAME_MAP.put((Object) ChatColor.GOLD + "Sn�en� po�kozen� zbrani", NAVYSENI_OBRANY);
			NAME_MAP.put((Object) ChatColor.GOLD + "Sn�en� po�kozen� kouzel", NAVYSENI_OBRANY_KOUZLA);
			NAME_MAP.put((Object) ChatColor.GREEN + "P�esnost", PRESNOST);
			NAME_MAP.put((Object) ChatColor.DARK_RED + "S�la", SILA);
			NAME_MAP.put((Object) ChatColor.DARK_GREEN + "S�la l��ebn�ch kouzel", SILA_LECEBNYCH_KOUZEL);
			NAME_MAP.put((Object) ChatColor.DARK_GREEN + "(M�sto pro klenot)", KLENOT);
			NAME_MAP.put((Object) ChatColor.BLUE + "Obrana", ARMOR);
			NAME_MAP.put((Object) ChatColor.DARK_GREEN + "Obratnost", OBRATNOST);
			
			for (Entry<String, DefaultAttributes> entry : NAME_MAP.entrySet()) {
				NAMES.put(entry.getValue(), entry.getKey());
			}
			NAME_MAP.forEach((x, y) -> {
				NOCOLOR_NAME_MAP.put(ChatColor.stripColor((String) x),
						(DefaultAttributes) y);
			});
		}

		private DefaultAttributes(String s) {
			this.s = s;
		}

		@Override
		public String toString() {
			return this.s;
		}

		public static String getName(DefaultAttributes stat) {
			if (stat == null) {
				return null;
			}
			return NAMES.get((Object) stat);
		}

		public static String getDefaultName(DefaultAttributes stat) {
			if (stat == null) {
				return null;
			}
			return stat.toString();
		}

		public static DefaultAttributes fromName(String name) {
			if (name == null) {
				return null;
			}
			for (DefaultAttributes attr : DefaultAttributes.values()) {
				if (attr.toString().equalsIgnoreCase(name)) {
					return attr;
				}
				if (!name.equalsIgnoreCase("obrana"))
					continue;
				return ARMOR;
			}
			return null;
		}
	}

}
