/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.drops.sell;

import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.drops.sell.Attribute.DefaultAttributes;
import git.doomshade.diablolike.utils.ItemUtils;

public class Formula {
	private final List<Attribute> attrs;
	private final List<Rarity> rarities;
	private final double perLevel;

	public Formula(List<Attribute> attrs, double perLevel, List<Rarity> rarities) {
		this.attrs = attrs;
		this.perLevel = perLevel;
		this.rarities = rarities;
	}

	public double getSellPrice(ItemStack item) {
		double value = 0.0;
		Map<DefaultAttributes, Integer> map = Attribute.getAttributes(item);
		for (Attribute.DefaultAttributes attr : map.keySet()) {
			for (Attribute attrr : attrs) {
				if (attrr.getAttribute() == null || attrr.getAttribute() != attr)
					continue;
				value += (double) map.get(attr).intValue() * attrr.getPrice();
			}
		}
		ItemUtils utils = new ItemUtils(item);
		if (utils.getItemLevel() < 0) {
			return Integer.MIN_VALUE;
		}
		value += perLevel * utils.getItemLevel();
		for (Rarity r : this.rarities) {
			if (!r.getTier().equals(utils.getTierColor()))
				continue;
			value *= r.getPrice();
			break;
		}
		return Math.ceil(value) < MainConfig.PRICECAP ? Math.ceil(value) : MainConfig.PRICECAP;
	}
}
