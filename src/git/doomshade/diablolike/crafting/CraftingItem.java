/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.MemorySection
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.configuration.serialization.ConfigurationSerializable
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.crafting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.utils.ItemUtils;

class CraftingItem implements ConfigurationSerializable {
	private ItemStack item;
	private int position;
	private String orb_cost;
	private int orb_amount;
	private static List<CraftingItem> items = new ArrayList<CraftingItem>();
	static File file;

	static boolean isCraftingItem(ItemStack item) {
		return true;
	}

	static List<CraftingItem> getAllItems() {
		return items;
	}

	private CraftingItem(ItemStack item, int position, String orb_cost, int orb_amount) {
		this.item = item;
		this.position = position;
		this.orb_cost = orb_cost;
		this.orb_amount = orb_amount;
	}

	private CraftingItem(Map<String, Object> map) {
		this.item = ItemStack.deserialize((Map<String, Object>) ((MemorySection) map.get("item")).getValues(true));
		this.position = (Integer) map.get("position");
		this.orb_cost = map.get("cost").toString();
		this.orb_amount = (Integer) map.get("amount");
	}

	ItemStack getItemStack() {
		return this.item;
	}

	static void test() {
		CraftingItem item = new CraftingItem(
				ItemUtils.makeItem(Material.STONE, "&aKEK", Arrays.asList("FU"), (short) 0), 0, "orb_vylepseni", 1);
		items.add(item);
		YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) file);
		loader.set(String.valueOf(loader.getKeys(false).size()), item.serialize());
		try {
			loader.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void load() {
		file = new File(Crafting.PATH, "items.yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		YamlConfiguration loader = YamlConfiguration.loadConfiguration((File) file);
		items = new ArrayList<CraftingItem>();
		for (String s : loader.getKeys(false)) {
			items.add(new CraftingItem(loader.getConfigurationSection(s).getValues(false)));
		}
	}

	public Map<String, Object> serialize() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("position", this.position);
		map.put("item", this.item.serialize());
		map.put("cost", this.orb_cost);
		map.put("amount", this.orb_amount);
		return map;
	}
}
