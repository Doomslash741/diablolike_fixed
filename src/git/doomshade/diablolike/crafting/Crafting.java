/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 */
package git.doomshade.diablolike.crafting;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import git.doomshade.diablolike.DiabloLike;

public class Crafting implements Listener {
	private static Crafting instance;
	private static CraftingConfig config;
	private static DiabloLike plugin;
	public static final File PATH;

	static {
		plugin = DiabloLike.getInstance();
		PATH = new File(plugin.getDataFolder() + File.separator + "crafting");
		instance = new Crafting();
	}

	public static Crafting getInstance() {
		return instance;
	}

	private static void load() {
		Crafting.reload();
		CraftingMenu.load();
	}

	public static void reload() {
		config = new CraftingConfig();
		try {
			config.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		CraftingItem.load();
		CraftingItem.test();
	}

	private Crafting() {
		Crafting.load();
	}

	public void onClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) {
			return;
		}
		String menuName = ChatColor.translateAlternateColorCodes((char) '&',
				(String) config.getLoader().getString("menu-name"));
		if (!e.getClickedInventory().getName().equalsIgnoreCase(menuName)) {
			return;
		}
		e.setCancelled(true);
	}

	public void open(Player hrac) {
		hrac.openInventory(CraftingMenu.getMenu());
	}
}
