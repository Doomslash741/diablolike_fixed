/*
 * Decompiled with CFR 0.139.
 */
package git.doomshade.diablolike.crafting;

import java.io.File;
import java.io.IOException;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.utils.Config;

class CraftingConfig extends Config {
	public CraftingConfig() {
		this.setInputStream(DiabloLike.getInstance().getResource("crafting config.yml"));
		this.setDirectory(Crafting.PATH);
		this.setFile(new File(Crafting.PATH, "crafting config.yml"));
		try {
			this.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
