package git.doomshade.diablolike.gui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class GUI extends GUIApi {
	private LinkedHashMap<Integer, Inventory> inventories;

	@Override
	protected final void setInventory(List<ItemStack> inv) {
		ArrayList<ItemStack> overLapping = new ArrayList<ItemStack>();
		this.inventories = new LinkedHashMap<>();
		if (this.name == null || this.name.isEmpty()) {
			this.name = (Object) ChatColor.GREEN + "Main";
		}
		int size = 9 - inv.size() % 9 + inv.size();
		int invAmount = 1;
		while (size > 51) {
			size -= 54;
			++invAmount;
		}
		for (int i = 0; i < invAmount; ++i) {
			overLapping = new ArrayList<>();
			Inventory inventory = Bukkit.createInventory(null,
					(int) (i == invAmount - 1 ? inv.size() + 3 + (9 - (inv.size() + 3) % 9) : 54), (String) this.name);
			if (invAmount != 1) {
				int prevPageIndex = inventory.getSize() - 3;
				int nextPageIndex = inventory.getSize() - 2;
				inventory.setItem(prevPageIndex, PREVIOUS_PAGE);
				inventory.setItem(nextPageIndex, NEXT_PAGE);
			}
			if (!this.getId().equalsIgnoreCase(GUIMain.MAINGUI.getId())) {
				int zpetButtonIndex = inventory.getSize() - 1;
				inventory.setItem(zpetButtonIndex, ZPET_BUTTON);
			}
			for (ItemStack item : inv) {
				if (inventory.firstEmpty() != -1) {
					inventory.addItem(new ItemStack[] { item });
					continue;
				}
				overLapping.add(item);
			}
			inv = new ArrayList<ItemStack>(overLapping);
			this.inventories.put(i, inventory);
		}
	}

	@Override
	public final LinkedHashMap<Integer, Inventory> getInventories() {
		return this.inventories;
	}
	
	
	@Override
	public final Inventory getInventory() {
		return this.inventories.get(0);
	}

}
