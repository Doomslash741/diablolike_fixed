/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 */
package git.doomshade.diablolike.gui;

import org.bukkit.ChatColor;

import git.doomshade.diablolike.gui.guis.DungeonGUI;
import git.doomshade.diablolike.gui.guis.GUIItem;
import git.doomshade.diablolike.gui.guis.KolekceGUI;
import git.doomshade.diablolike.gui.guis.MainGUI;

public class GUIMain {
	public static MainGUI MAINGUI;

	public static void init() {
		MAINGUI = new MainGUI();
		GUIApi.registerGUI(MAINGUI);
		GUIApi.registerGUI(new GUIItem());
		GUIApi.registerGUI(new KolekceGUI());
		GUIApi.registerGUI(new DungeonGUI());
		
		
		for (GUIApi gui : GUIApi.GUIS_BY_ID.values()) {
			gui.init();
		}
		for (GUIApi gui : GUIApi.GUIS_BY_ID.values()) {
			GUIApi.GUIS_BY_NAME.put(ChatColor.stripColor((String) gui.name), gui);
		}
	}
}
