/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.event.EventHandler
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.gui.guis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.gui.GUI;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class DungeonGUI extends GUI {
	@EventHandler
	@Override
	protected void onClick(GUIClickEvent e) {
		if (!this.isValid(e)) {
			return;
		}
	}

	@Override
	public String getId() {
		return "dungeons";
	}

	@Override
	public List<ItemStack> setupInventory() {
		LinkedList<ItemStack> inv = new LinkedList<ItemStack>();
		for (Dungeon kolekce : Utils.DUNGEONS.values()) {
			if (kolekce == null || kolekce.getName() == null || kolekce.getName().isEmpty())
				continue;
			inv.add(ItemUtils.makeItem(Material.CHEST, kolekce.getName(), new ArrayList<String>(), (short) 0));
		}
		return inv;
	}

	@Override
	public ItemStack setupClickedItem() {
		return ItemUtils.makeItem(Material.CHEST, (Object) ChatColor.GREEN + "Dungeons",
				Arrays.asList("Otevre menu s kolekcemi"), (short) 0);
	}
}
