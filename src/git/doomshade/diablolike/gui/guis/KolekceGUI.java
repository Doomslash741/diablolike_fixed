/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.gui.guis;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.gui.GUI;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class KolekceGUI extends GUI {
	@EventHandler
	@Override
	protected void onClick(GUIClickEvent e) {
		if (!this.isValid(e)) {
			return;
		}
		String item = e.getEvent().getCurrentItem().getItemMeta().getDisplayName();
		if (!Utils.COLLECTIONS.keySet().contains(item)) {
			return;
		}
		Player hrac = (Player) e.getEvent().getWhoClicked();
		Kolekce kolekce = new Kolekce(item);
		GUIItem guiItem = (GUIItem) getGuiById("guiitem");
		File file = kolekce.getFile();
		guiItem.set(file, "");
		openGui(hrac, guiItem);
		resetPage(hrac);
	}

	@Override
	public String getId() {
		return "kolekce";
	}

	@Override
	public List<ItemStack> setupInventory() {
		LinkedList<ItemStack> inv = new LinkedList<ItemStack>();
		for (Kolekce kolekce : Utils.COLLECTIONS.values()) {
			if (kolekce == null || kolekce.getName() == null || kolekce.getName().isEmpty())
				continue;
			inv.add(ItemUtils.makeItem(Material.CHEST, kolekce.getName(), new ArrayList<String>(), (short) 0));
		}
		return inv;
	}

	@Override
	public ItemStack setupClickedItem() {
		return ItemUtils.makeItem(Material.CHEST, (Object) ChatColor.GREEN + "Kolekce",
				Arrays.asList("Otevre menu s kolekcemi"), (short) 0);
	}
}
