/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.inventory.InventoryAction
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.gui.guis;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.gui.GUI;
import git.doomshade.diablolike.gui.GUIApi;

public class MainGUI extends GUI {
	@EventHandler
	@Override
	protected void onClick(GUIClickEvent event) {
		if (!this.isValid(event)) {
			return;
		}
		InventoryClickEvent e = event.getEvent();
		ItemStack item = e.getCurrentItem();
		if (e.getAction() != InventoryAction.PICKUP_ALL || item == null) {
			return;
		}
		GUIApi clickedGui = getGuiByName(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
		if (clickedGui == null) {
			return;
		}
		Player hrac = (Player) e.getWhoClicked();
		openGui(hrac, clickedGui);
		resetPage(hrac);
	}

	@Override
	public String getId() {
		return "main";
	}

	@Override
	public List<ItemStack> setupInventory() {
		LinkedList<ItemStack> inv = new LinkedList<ItemStack>();
		inv.add(getGuiById("kolekce").getClickedItem());
		inv.add(getGuiById("dungeons").getClickedItem());
		return inv;
	}

	@Override
	public ItemStack setupClickedItem() {
		return null;
	}
}
