package git.doomshade.diablolike.gui.guis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.config.Chance;
import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.gui.GUI;
import git.doomshade.diablolike.listeners.GUIListener;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.utils.Utils;

public class GUIItem extends GUI {
	private File file;
	private String section;
	private static final HashMap<Player, String> ID = new HashMap<>();
	private static final String NASTAVIT = "Nastavit ";

	public GUIItem() {
	}

	@Override
	@EventHandler
	protected void onClick(GUIClickEvent e) {
		if (!isValid(e)) {
			return;
		}
		Player hrac = (Player) e.getEvent().getWhoClicked();
		if (file == null) {
			hrac.sendMessage("Kontaktuj prosim admina (file == null).");
			return;
		}
		String dName = e.getEvent().getCurrentItem().getItemMeta().getDisplayName();
		String id = dName.replaceAll("Nastavit ", "").replaceAll("Pridat ", "");
		ID.put(hrac, id);
		hrac.closeInventory();
		switch (dName) {
		case NASTAVIT + " item":
			hrac.sendMessage("Nyni napis config jmeno itemu");
			hrac.sendMessage(YamlConfiguration.loadConfiguration(file).getKeys(false) + "");
			break;
		case PRIDAT + " predmet":
			hrac.sendMessage("Nyni napis config jmeno itemu");
			hrac.sendMessage("UNAVAILABLE: ");
			hrac.sendMessage(YamlConfiguration.loadConfiguration(file).getKeys(false) + "");
			break;
		}
		hrac.sendMessage("Nyni napis hodnotu");

		hrac.sendMessage("Pokud ses misclicknul, napis \"" + REDO + "\"");
		PLAYERS_CHAT.add(hrac.getUniqueId());
		GUIListener.PREVIOUS_GUIS.removeLast();
		resetPage(hrac);
	}

	private static final String REDO = "redo";
	private static final String PRIDAT = "Pridat ";

	@Override
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		super.onChat(e);
		Player hrac = e.getPlayer();
		if (!PLAYERS_CHAT.contains(hrac.getUniqueId())) {
			return;
		}
		String m = e.getMessage();
		if (m.equalsIgnoreCase(REDO)) {
			reopenInventory(hrac);
			return;
		}

		Object value = null;
		String id = (String) ID.get(hrac);

		if (Chance.isChance(id)) {
			value = Double.valueOf(0.0D);
			try {
				value = Double.valueOf(Double.parseDouble(m));
			} catch (NumberFormatException e1) {
				hrac.sendMessage("Spatne jsi napsal cislo, zkus znovu.");
				return;
			}
		} else {
			if (id.equalsIgnoreCase("item")) {
				if (!Utils.DIABLO_ITEMS_BY_CONFIGNAME.containsKey(m)) {
					hrac.sendMessage("Spatne jsi napsal config jmeno, zkus znovu.");
					return;
				}
				setSection(m);
				reopenInventory(hrac);
				return;
			}
			if (id.equalsIgnoreCase("predmet")) {
				if (Utils.DIABLO_ITEMS_BY_CONFIGNAME.containsKey(m)) {
					hrac.sendMessage("Toto config jmeno jiz existuje. Vyber jine.");
					return;
				}
				value = hrac.getInventory().getItemInMainHand();
				if (value == null) {
					hrac.sendMessage("Musis mit neco v ruce.");
					return;
				}
				setSection(m);

			} else {
				hrac.sendMessage(id);
				value = m;
			}
		}
		try {
			setValue(id, value);
			ID.remove(hrac);
			reopenInventory(hrac);
			hrac.sendMessage(id + " uspesne zapsan do souboru " + file.getName());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void reopenInventory(Player hrac) {
		PLAYERS_CHAT.remove(hrac.getUniqueId());
		hrac.openInventory(getInventory());
	}

	@Override
	public String getId() {
		return "guiitem";
	}

	private void setSection(String section) {
		set(file, section);
	}

	public void set(File file, String section) {
		this.file = file;
		this.section = section;
	}

	private void setValue(String id, Object value) throws IOException {
		FileConfiguration loader = YamlConfiguration.loadConfiguration(file);
		if (this.section.isEmpty()) {
			loader.set(id, value);
		} else if ((value instanceof ItemStack)) {
			ItemStack item = (ItemStack) value;
			ConfigurationSection section = loader.createSection(this.section);
			if ((item.hasItemMeta()) && ((item.getItemMeta() instanceof org.bukkit.inventory.meta.SkullMeta))) {
				section.set("item", item.serialize());
			} else {
				section.set("materialName", item.getType().toString().toUpperCase());
				section.set("displayName", "");
				section.set("lore", Arrays.asList(new String[] { "" }));
				if (item.hasItemMeta()) {
					ItemMeta meta = item.getItemMeta();
					if (meta.hasDisplayName()) {
						section.set("displayName", meta.getDisplayName().replaceAll("�", "&"));
					}
					if (meta.hasLore()) {
						List<String> lore = new ArrayList<>(meta.getLore());
						for (int i = 0; i < lore.size(); i++) {
							lore.set(i, ((String) lore.get(i)).replaceAll("�", "&"));
						}
						section.set("lore", lore);
					}
				}
			}
		} else {
			loader.getConfigurationSection(this.section).set(id, value);
		}
		loader.save(file);
	}

	@Override
	public List<ItemStack> setupInventory() {
		List<ItemStack> inv = new LinkedList<ItemStack>();
		for (Chance c : Chance.values()) {
			if (c != Chance.COMMON) {

				inv.add(ItemUtils.makeItem(Material.EYE_OF_ENDER, "Nastavit " + c.toString(), new ArrayList<>(),
						(short) 0));
			}
		}
		inv.add(ItemUtils.makeItem(Material.SHIELD, "Nastavit item", new ArrayList<>(), (short) 0));
		inv.add(ItemUtils.makeItem(Material.WOOD_AXE, "Pridat predmet",
				Arrays.asList(new String[] { "Prida predmet, ktery prave drzis v ruce, do souboru" }), (short) 0));

		return inv;
	}

	@Override
	public ItemStack setupClickedItem() {
		return ItemUtils.makeItem(Material.ACACIA_DOOR, ChatColor.GREEN + "Nastaveni", new ArrayList<>(), (short) 0);
	}
}
