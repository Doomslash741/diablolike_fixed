/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 */
package git.doomshade.diablolike.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.utils.ItemUtils;

public abstract class GUIApi implements Listener {
	protected static final ArrayList<UUID> PLAYERS_CHAT = new ArrayList<>();
	protected ItemStack clickedItem = null;
	public static final HashMap<String, GUIApi> GUIS_BY_ID = new HashMap<>();
	public static final HashMap<String, GUIApi> CUSTOM_GUIS_BY_ID = new HashMap<>();
	public static final HashMap<String, GUIApi> GUIS_BY_NAME = new HashMap<>();
	public static final ItemStack ZPET_BUTTON = ItemUtils.makeItem(Material.BARRIER, (Object) ChatColor.RED + "Zpet",
			new ArrayList<String>(), (short) 0);

	public static final HashMap<UUID, Integer> CURRENT_PAGE = new HashMap<>();
	protected String name;
	public static final ItemStack PREVIOUS_PAGE = ItemUtils.makeItem(Material.BOOK, "Predesla stranka",
			new ArrayList<String>(), (short) 0);
	public static final ItemStack NEXT_PAGE = ItemUtils.makeItem(Material.BOOK, "Dalsi stranka",
			new ArrayList<String>(), (short) 0);

	public void init() {
		this.setClickedItem(this.setupClickedItem());
		this.setInventory(this.setupInventory());
	}

	protected abstract void setInventory(List<ItemStack> list);

	public abstract List<ItemStack> setupInventory();

	public abstract ItemStack setupClickedItem();

	@EventHandler
	protected abstract void onClick(GUIClickEvent var1);

	protected final void setClickedItem(ItemStack item) {
		this.clickedItem = item;
		if (this.clickedItem != null && this.clickedItem.hasItemMeta()
				&& this.clickedItem.getItemMeta().hasDisplayName()) {
			this.name = this.clickedItem.getItemMeta().getDisplayName();
		}
	}
	
	protected void resetPage(Player hrac) {
		CURRENT_PAGE.put(hrac.getUniqueId(), 0);
	}
	
	protected void openGui(Player hrac, GUIApi gui) {
		hrac.openInventory(gui.getInventory());
	}

	public abstract String getId();

	public final ItemStack getClickedItem() {
		return this.clickedItem;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player hrac = e.getPlayer();
		if (!PLAYERS_CHAT.contains(hrac.getUniqueId())) {
			return;
		}
		e.setCancelled(true);
	}

	protected final boolean isValid(GUIClickEvent e) {
		Inventory inv = e.getEvent().getClickedInventory();
		return ChatColor.stripColor((String) inv.getName()).equalsIgnoreCase(ChatColor.stripColor((String) this.name));
	}

	public abstract Inventory getInventory();

	public abstract LinkedHashMap<Integer, Inventory> getInventories();

	public static void registerGUI(GUIApi gui) {
		Bukkit.getPluginManager().registerEvents(gui, DiabloLike.getInstance());
		GUIS_BY_ID.put(gui.getId(), gui);
	}

	public static GUIApi getGuiById(String id) {
		return GUIS_BY_ID.get(id);
	}

	public static GUIApi getGuiByName(String name) {
		return GUIS_BY_NAME.get(name);
	}

	public static GUIApi getGui(Inventory inv) {
		return getGuiByName(ChatColor.stripColor(inv.getName()));
	}

	public static boolean isGuiById(String id) {
		return GUIS_BY_ID.containsKey(id);
	}

	public static boolean isGuiByName(String name) {
		return GUIS_BY_NAME.containsKey(ChatColor.stripColor((String) name));
	}

	public static boolean isGui(Inventory inv) {
		return isGuiByName(inv.getName());
	}
}
