/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.inventory.InventoryCloseEvent
 *  org.bukkit.event.inventory.InventoryMoveItemEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerPickupItemEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 */
package git.doomshade.diablolike.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.cmds.CustomCommand;
import git.doomshade.diablolike.cmds.ItemInfoCommand;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.identifying.Identifier;
import git.doomshade.diablolike.identifying.Identify;
import git.doomshade.diablolike.utils.ItemUtils;

public class PlayerActions implements Listener {
	public static Map<Player, Identifier> IDENTIFYING = new HashMap<Player, Identifier>();
	private HashMap<Player, Location> lastLocation = new HashMap<Player, Location>();

	private boolean movedPast(Player player, int distance) {
		if (!this.lastLocation.containsKey((Object) player)) {
			return true;
		}
		return this.lastLocation.get((Object) player).distance(player.getLocation()) > (double) distance;
	}

	public static boolean isIdentifying(Player hrac) {
		return IDENTIFYING.containsKey((Object) hrac);
	}

	@EventHandler
	public void onMenuC(InventoryMoveItemEvent e) {
		if (e.getInitiator().getName().equalsIgnoreCase(ItemInfoCommand.MENUNAME)
				|| e.getDestination().getName().equalsIgnoreCase(ItemInfoCommand.MENUNAME)) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onMenuClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase(ItemInfoCommand.MENUNAME)) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		PlayerActions.up((Player) e.getWhoClicked());
	}

	public void onMove(PlayerMoveEvent e) {
		if (!this.movedPast(e.getPlayer(), 5)) {
			return;
		}
		this.lastLocation.put(e.getPlayer(), e.getTo());
		for (ItemStack item : e.getPlayer().getInventory().getContents()) {
			if (item == null)
				continue;
			Bukkit.getConsoleSender().sendMessage("" + item.getItemMeta().getLore());
		}
		PlayerActions.up(e.getPlayer());
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getTitle()
				.equalsIgnoreCase("Prebytecne predmety - " + ((Player) e.getPlayer()).getDisplayName())) {
			CustomCommand.INVENTORIES.put(e.getPlayer().getUniqueId(), e.getInventory());
		}
		PlayerActions.up((Player) e.getPlayer());
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		PlayerActions.up(e.getPlayer());
	}

	private static void up(final Player p) {
		new BukkitRunnable() {

			@Override
			public void run() {
				for (ItemStack item : p.getInventory().getContents()) {
					ItemUtils utils = new ItemUtils(item);
					utils.updatePrice();
				}
				ItemStack[] contents = p.getInventory().getArmorContents();
				for (int i = 0; i < contents.length; ++i) {
					if (contents[i] == null || new Identify(contents[i]).isIdentified())
						continue;
					ItemStack item = contents[i];
					if (p.getInventory().firstEmpty() == -1) {
						p.getWorld().dropItem(p.getLocation(), item);
					} else {
						p.getInventory().addItem(new ItemStack[] { contents[i] });
					}
					contents[i] = null;
				}
				p.getInventory().setArmorContents(contents);
				this.cancel();
			}
		}.runTaskLater((Plugin) DiabloLike.getInstance(), 1L);
	}

	@EventHandler
	public void identify(PlayerInteractEvent e) {
		Player hrac = e.getPlayer();
		PlayerInventory inv = hrac.getInventory();
		if (inv.getItemInMainHand() == null) {
			return;
		}
		Action ac = e.getAction();
		if (ac != Action.RIGHT_CLICK_AIR && ac != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		ItemStack mh = inv.getItemInMainHand();
		if (mh == null) {
			return;
		}
		if (Identifier.isIdentifier(mh)) {
			IDENTIFYING.put(e.getPlayer(), Identifier.getIdentifier(mh));
			hrac.sendMessage(Messages.NOW_IDENTIFIYING.getMessage());
			return;
		}
		if (!isIdentifying(hrac)) {
			return;
		}
		if (!isCorrectAmount(mh)) {
			hrac.sendMessage(Messages.TOO_MANY_ITEMS.getMessage(mh.getItemMeta().getDisplayName()));
			return;
		}
		Identifier id = IDENTIFYING.get(hrac);
		IDENTIFYING.remove(hrac);
		if (!id.canIdentify(mh)) {
			String name = mh.hasItemMeta()
					? (mh.getItemMeta().hasDisplayName() ? mh.getItemMeta().getDisplayName() : mh.getType().toString())
					: mh.getType().toString();
			hrac.sendMessage(Messages.NOT_IDENTIFIABLE.getMessage(name));
			return;
		}
		if (!inv.contains(id.getItem())) {
			return;
		}
		id.toIdentify(mh).setIdentified(true);
		hrac.sendMessage(Messages.IDENTIFIED.getMessage(mh.getItemMeta().getDisplayName()));
		ItemStack item = id.getItem().clone();
		item.setAmount(1);
		hrac.getInventory().removeItem(new ItemStack[] { item });

	}

	private static boolean isCorrectAmount(ItemStack item) {
		return item.getAmount() == 1;
	}

}
