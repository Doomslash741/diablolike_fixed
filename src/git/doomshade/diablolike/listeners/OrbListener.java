/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.Messages;
import git.doomshade.diablolike.orbs.Orb;
import git.doomshade.diablolike.orbs.OrbType;
import git.doomshade.diablolike.orbs.orb.OrbVylepseni2;
import git.doomshade.diablolike.utils.ItemUtils;

public class OrbListener implements Listener {
	private static Map<Player, Boolean> rightClicks = new HashMap<Player, Boolean>();
	private static Map<Player, Orb> orb = new HashMap<Player, Orb>();

	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player hrac = e.getPlayer();
			if (!rightClicks.containsKey((Object) hrac)) {
				rightClicks.put(hrac, false);
			}
			ItemStack item = e.getItem();
			ItemUtils utils = new ItemUtils(item);
			OrbType orb = null;
			for (OrbType or : OrbType.values()) {
				if (!or.getOrb().getOrbItem().isSimilar(item))
					continue;
				orb = or;
				OrbListener.orb.put(hrac, orb.getOrb());
				break;
			}
			if (OrbListener.orb.get((Object) hrac) == null && !this.isRightClicked(hrac)) {
				return;
			}
			if (OrbListener.orb.get((Object) hrac).getOrbItem().isSimilar(item)) {
				rightClicks.put(hrac, true);
				hrac.sendMessage(Messages.NOW_GEMMING.getMessage(item.getItemMeta().getDisplayName()));
				return;
			}
			hrac.sendMessage("a");
			if (!this.isRightClicked(hrac)) {
				return;
			}
			hrac.sendMessage("a");
			if (utils.getAttributes().isEmpty()) {
				return;
			}
			hrac.sendMessage("a");
			if (item.getAmount() != 1) {
				hrac.sendMessage(Messages.TOO_MANY_ITEMS_GEM.getMessage(item.getItemMeta().getDisplayName()));
				return;
			}
			if (OrbListener.orb.get((Object) hrac) instanceof OrbVylepseni2
					&& OrbVylepseni2.pocetVylepseni(item) >= MainConfig.MAX_VYLEPSENI) {
				hrac.sendMessage(Messages.MAX_VYLEPSENI.getMessage(item.getItemMeta().getDisplayName()));
				return;
			}
			if (OrbListener.orb.get((Object) hrac).triggerOrb(item)) {
				hrac.getInventory().removeItem(new ItemStack[] { OrbListener.orb.get((Object) hrac).getOrbItem() });
				OrbListener.orb.put(hrac, null);
				hrac.sendMessage(Messages.GEMMED.getMessage(item.getItemMeta().getDisplayName()));
			}
		}
	}

	private boolean isRightClicked(Player p) {
		return rightClicks.get((Object) p);
	}
}
