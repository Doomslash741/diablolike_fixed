/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Event
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.listeners;

import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.events.GUIClickEvent;
import git.doomshade.diablolike.gui.GUIApi;

public class GUIListener implements Listener {
	public static final LinkedList<String> PREVIOUS_GUIS = new LinkedList<String>();

	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		Inventory inv = e.getClickedInventory();
		ItemStack item = e.getCurrentItem();
		if (inv == null || !GUIApi.isGui(inv) || item == null) {
			return;
		}
		

		Player hrac = (Player) e.getWhoClicked();
		if (!GUIApi.CURRENT_PAGE.containsKey(hrac.getUniqueId())) {
			return;
		}
		e.setCancelled(true);
		int updated = GUIApi.CURRENT_PAGE.get(hrac.getUniqueId());
		GUIApi gui = GUIApi.getGui(inv);
		int max = gui.getInventories().size() - 1;
		if (item.isSimilar(GUIApi.ZPET_BUTTON)) {
			GUIApi lastGui = GUIApi.getGuiById(PREVIOUS_GUIS.removeLast());
			hrac.openInventory(lastGui.getInventory());
		} else if (item.isSimilar(GUIApi.NEXT_PAGE)) {
			if (updated == max) {
				updated = -1;
			}
			GUIApi.CURRENT_PAGE.put(hrac.getUniqueId(), ++updated);
			hrac.openInventory(gui.getInventories().get(updated));
		} else if (item.isSimilar(GUIApi.PREVIOUS_PAGE)) {
			if (updated == 0) {
				updated = max + 1;
			}
			GUIApi.CURRENT_PAGE.put(hrac.getUniqueId(), --updated);
			hrac.openInventory(gui.getInventories().get(updated));
		} else {
			if (!PREVIOUS_GUIS.contains(gui.getId())) {
				PREVIOUS_GUIS.add(gui.getId());
			}
			Bukkit.getPluginManager().callEvent((Event) new GUIClickEvent(e));
		}
	}
}
