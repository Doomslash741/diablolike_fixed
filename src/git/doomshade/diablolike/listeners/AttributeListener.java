/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 */
package git.doomshade.diablolike.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.cmds.DebugCommand;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.config.MainConfig.ArmorType;
import git.doomshade.diablolike.utils.ItemUtils;

public class AttributeListener implements Listener {
	private static final Pattern OBRANA_PATTERN = Pattern.compile("[+0-9]+ Obrana");

	@EventHandler(priority = EventPriority.HIGH)
	public void onDamageTaken(EntityDamageByEntityEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			return;
		}
		// e.setDamage(DamageModifier.BASE, e.getFinalDamage() * getObrana((Player)
		// e.getEntity()));
		Player hrac = (Player) e.getEntity();
		double obrana = getObrana(hrac);
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage("Obrana value: " + obrana);
			hrac.sendMessage("Damage taken: " + e.getDamage());
			hrac.sendMessage("Damage taken after reductions: " + e.getFinalDamage());
		}
		e.setDamage(e.getDamage() * obrana);
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage("Damage taken after reductions with Obrana: " + e.getFinalDamage());
			hrac.sendMessage(ChatColor.RED + "-------DAMAGE TAKEN-------");
		}
	}

	public static boolean canAdd(ItemStack item, String name) {
		return item != null && new ItemUtils(item).isValid(true, false)
				&& ChatColor.stripColor(item.getItemMeta().getDisplayName().toLowerCase()).contains(name.toLowerCase());
	}

	public static double getObrana(Player hrac) {
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage("");
			hrac.sendMessage(ChatColor.GREEN + "-------DAMAGE TAKEN-------");
		}
		double obrana = getCelkemObrana(hrac);

		double val = MainConfig.OBRANA;
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage("Celkem obrany: " + obrana);
			hrac.sendMessage("Config value: " + val);
		}
		obrana /= (obrana + val);
		return 1.0 - obrana;
	}

	private static ArmorType getArmorType(ItemStack item) {
		switch (item.getType()) {

		// to tu ani nemus� b�t, ale kdyby n�hodou..
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
			return ArmorType.LEATHER;
		//

		case GOLD_HELMET:
		case GOLD_CHESTPLATE:
		case GOLD_LEGGINGS:
		case GOLD_BOOTS:
			return ArmorType.GOLD;
		case CHAINMAIL_HELMET:
		case CHAINMAIL_CHESTPLATE:
		case CHAINMAIL_LEGGINGS:
		case CHAINMAIL_BOOTS:
			return ArmorType.CHAINMAIL;
		case IRON_HELMET:
		case IRON_CHESTPLATE:
		case IRON_LEGGINGS:
		case IRON_BOOTS:
			return ArmorType.IRON;
		case DIAMOND_HELMET:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_LEGGINGS:
		case DIAMOND_BOOTS:
			return ArmorType.DIAMOND;
		default:
			return ArmorType.LEATHER;
		}
	}

	public static double getObranaMultiplier(ItemStack item) {
		return MainConfig.getObranaMultiplier(getArmorType(item));
	}

	public static int getCelkemObrana(Player hrac) {
		ItemStack nahrdelnik;
		double obrana = 0;
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		items.addAll(Arrays.asList(hrac.getInventory().getArmorContents()));
		items.add(hrac.getInventory().getItemInMainHand());
		ItemStack offHand = hrac.getInventory().getItemInOffHand();

		// fuck this honestly
		if (canAdd(offHand, "Offhand")) {
			items.add(offHand);
		}
		if (canAdd(nahrdelnik = hrac.getInventory().getItem(9), "prsten")) {
			items.add(nahrdelnik);
		}
		if (canAdd(nahrdelnik = hrac.getInventory().getItem(9), "prst�nek")) {
			items.add(nahrdelnik);
		}

		if (canAdd(nahrdelnik = hrac.getInventory().getItem(9), "prstynek")) {
			items.add(nahrdelnik);
		}

		if (canAdd(nahrdelnik = hrac.getInventory().getItem(10), "prsten")) {
			items.add(nahrdelnik);
		}

		if (canAdd(nahrdelnik = hrac.getInventory().getItem(10), "prst�nek")) {
			items.add(nahrdelnik);
		}
		if (canAdd(nahrdelnik = hrac.getInventory().getItem(10), "prstynek")) {
			items.add(nahrdelnik);
		}

		if (canAdd(nahrdelnik = hrac.getInventory().getItem(11), "nahrdelnik")) {
			items.add(nahrdelnik);
		}

		if (canAdd(nahrdelnik = hrac.getInventory().getItem(11), "n�hrdeln�k")) {
			items.add(nahrdelnik);
		}

		ArmorType at = ArmorType.LEATHER;
		for (ItemStack item : items) {
			if (item == null || !new ItemUtils(item).isValid(false, true))
				continue;
			for (String x : item.getItemMeta().getLore()) {
				if (x == "" || !OBRANA_PATTERN.matcher(ChatColor.stripColor((String) x)).find())
					continue;
				double itemObrana = Double.parseDouble(ChatColor.stripColor(x).replaceAll("[\\D]", "").trim());
				ArmorType atItem = getArmorType(item);
				if (at.ordinal() < atItem.ordinal())
					at = atItem;
				obrana += itemObrana;
			}
		}
		
		obrana *= MainConfig.getObranaMultiplier(at);
		return (int) Math.round(obrana);
	}
}
