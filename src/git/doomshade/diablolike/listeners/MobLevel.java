/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.command.ConsoleCommandSender
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.entity.EntityDeathEvent
 *  org.bukkit.inventory.ItemStack
 */
package git.doomshade.diablolike.listeners;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.cmds.DebugCommand;
import git.doomshade.diablolike.drops.Drop;
import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.events.DropEvent;
import git.doomshade.diablolike.utils.Utils;

public class MobLevel implements Listener {
	private static final Pattern pattern = Pattern.compile("[Lv.]+ [0-9]+");

	private static int getMobLevel(String mobName) {
		int level = Integer.MIN_VALUE;
		Matcher m = pattern.matcher(mobName);
		if (m.find()) {
			for (String s : m.group(0).split(" ")) {
				try {
					level = Integer.parseInt(s);
					break;
				} catch (NumberFormatException numberFormatException) {
				}
			}
		}
		return level;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onMobKill(EntityDeathEvent e) {
		int level;
		Kolekce kolekce;
		LivingEntity ent = e.getEntity();
		if (ent.getKiller() == null || ent.getCustomName() == null) {
			return;
		}
		Player hrac = ent.getKiller();
		String mobName = ChatColor.stripColor((String) ent.getCustomName());
		Location lokace = ent.getLocation();
		World svet = lokace.getWorld();
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		HashMap<String, String> dungMobNames = new HashMap<String, String>(Utils.DUNGEON_MOB_NAMES);
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage("");
			hrac.sendMessage(ChatColor.GREEN + "-------DROP-------");
		}
		if (dungMobNames.keySet().contains(mobName)) {
			String cfgMobName = (String) dungMobNames.get(mobName);
			if (DebugCommand.DEBUG && hrac.isOp()) {
				hrac.sendMessage("(Dungeon) Found mob - mob exists in a dungeon (" + cfgMobName + ")");
			}
			console.sendMessage("(MobLevel) FOUND MOB (" + cfgMobName + ")");
			for (Dungeon dungeon : Utils.DUNGEONS.values()) {
				Map<String, List<Kolekce>> drops = dungeon.getDrops();
				if (!drops.keySet().contains(cfgMobName))
					continue;
				if (DebugCommand.DEBUG && hrac.isOp()) {
					hrac.sendMessage("(Dungeon) Found its dungeon (" + dungeon.getName() + ")");
					hrac.sendMessage("(Dungeon) Dropping collections");
				}
				console.sendMessage("(MobLevel) FOUND DUNG (" + dungeon.getName() + ")");

				for (Kolekce kolekce2 : drops.get(cfgMobName)) {
					if (DebugCommand.DEBUG && hrac.isOp()) {
						hrac.sendMessage(kolekce2.toString());
					}
					console.sendMessage(kolekce2.toString());
					List<ItemStack> randomDrop = kolekce2.getRandomDrop();
					if (DebugCommand.DEBUG && hrac.isOp()) {
						hrac.sendMessage("(Dungeon) Chosen drop: " + randomDrop);
					}
					for (ItemStack item : randomDrop) {
						if (item == null)
							continue;
						svet.dropItem(lokace, item);
						if (DebugCommand.DEBUG && hrac.isOp()) {
							hrac.sendMessage("(Dungeon) Dropping item");
							hrac.sendMessage(item.getItemMeta().getDisplayName());
						}
						Bukkit.getPluginManager().callEvent(new DropEvent(item));
					}
				}
			}
		}
		if (Utils.MOB_NAMES.keySet().contains(mobName)
				&& (kolekce = DiabloLike.getCollection(Utils.MOB_NAMES.get(mobName))).exists()) {
			if (DebugCommand.DEBUG && hrac.isOp()) {
				hrac.sendMessage("(Collection) Found custom collection for mob (" + kolekce.getName() + ")");
				hrac.sendMessage(kolekce.toString());
			}
			console.sendMessage("(MobLevel) FOUND CUSTOM COLECTION (" + kolekce.getName() + ")");
			console.sendMessage(kolekce.toString());
			List<ItemStack> chosenDrop = kolekce.getRandomDrop();
			if (DebugCommand.DEBUG && hrac.isOp()) {
				hrac.sendMessage("(Collection) Chosen drop: " + chosenDrop);
			}
			for (ItemStack item : chosenDrop) {
				if (item == null)
					continue;
				svet.dropItem(lokace, item);
				if (DebugCommand.DEBUG && hrac.isOp()) {
					hrac.sendMessage("(Collection) Dropping item");
					hrac.sendMessage(item.getItemMeta().getDisplayName());
				}
				Bukkit.getPluginManager().callEvent(new DropEvent(item));
			}
		}
		if ((level = MobLevel.getMobLevel(mobName)) != Integer.MIN_VALUE) {
			if (DebugCommand.DEBUG && hrac.isOp()) {
				hrac.sendMessage("(Mob level) Mob has a level (" + level + ")");
			}
			console.sendMessage("(MobLevel) FOUND MOB LEVEL (" + level + ")");
			ItemStack drop = new Drop(level).drop();
			if (drop != null) {
				svet.dropItem(lokace, drop);
				if (DebugCommand.DEBUG && hrac.isOp()) {
					hrac.sendMessage("(Mob level) Dropping " + drop.getItemMeta().getDisplayName());
				}
				Bukkit.getPluginManager().callEvent(new DropEvent(drop));
			} else {
				if (DebugCommand.DEBUG && hrac.isOp()) {
					hrac.sendMessage("(Mob level) Mob has not dropped anything");
				}
			}
		}
		if (DebugCommand.DEBUG && hrac.isOp()) {
			hrac.sendMessage(ChatColor.RED + "-------DROP-------");
		}
	}
}
