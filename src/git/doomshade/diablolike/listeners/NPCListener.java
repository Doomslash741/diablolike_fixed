/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  net.milkbowl.vault.economy.Economy
 *  net.milkbowl.vault.economy.EconomyResponse
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.inventory.InventoryCloseEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 */
package git.doomshade.diablolike.listeners;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import git.doomshade.diablolike.DiabloLike;
import git.doomshade.diablolike.config.MainConfig;
import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.diablolike.vendors.VendorTrait;

public class NPCListener implements Listener {
	@EventHandler
	public void onEscape(InventoryCloseEvent e) {
		if (e.getInventory().getTitle().equalsIgnoreCase(MainConfig.VENDOR_INVENTORY_NAME)) {
			decline((Player) e.getPlayer(), e.getInventory());
		}
	}

	@EventHandler
	public void onItemAdd(final InventoryClickEvent e) {
		if (e.getClickedInventory() == null) {
			return;
		}
		final Player hrac = (Player) e.getWhoClicked();
		if (!e.getClickedInventory().getTitle().equalsIgnoreCase(MainConfig.VENDOR_INVENTORY_NAME)) {
			return;
		}
		new BukkitRunnable() {

			@Override
			public void run() {
				updateMoney(hrac, NPCListener.getMoney(e));
			}
		}.runTaskLater(DiabloLike.getInstance(), 1L);
		if (e.getRawSlot() % 9 <= 3) {
			return;
		}
		e.setCancelled(true);
		ItemStack accept = MainConfig.getVendorAcceptButtonItem();
		ItemStack decline = MainConfig.getVendorDeclineButtonItem();
		ItemStack item = e.getCurrentItem();
		if (!ItemUtils.isValid(item, true, false)) {
			return;
		}
		if (item.isSimilar(accept)) {
			accept(hrac, NPCListener.getMoney(e));
			return;
		}
		if (item.isSimilar(decline)) {
			hrac.closeInventory();
			return;
		}
	}

	private void decline(Player hrac, Inventory inventory) {
		for (int i = 0; i < 54; ++i) {
			ItemStack item = inventory.getItem(i);
			if (item == null || i % 9 > 3)
				continue;
			hrac.getInventory().addItem(new ItemStack[] { item });
		}
	}

	private void updateMoney(Player hrac, double money) {
		if (hrac.getOpenInventory() != null
				&& !Arrays.asList(hrac.getOpenInventory().getTopInventory().getStorageContents()).isEmpty()
				&& hrac.getOpenInventory().getTopInventory().getSize() > 15) {
			hrac.getOpenInventory().getTopInventory().setItem(MainConfig.VENDOR_MONEY_POSITION,
					VendorTrait.moneyInfoItem(money));
		}
	}

	private void accept(Player hrac, double money) {
		hrac.getOpenInventory().getTopInventory().clear();
		hrac.closeInventory();
		DiabloLike.getInstance().getEconomy().depositPlayer((OfflinePlayer) hrac, money);
		hrac.sendMessage((Object) ChatColor.GREEN + "Bylo ti pripsano " + (Object) ChatColor.DARK_AQUA + money
				+ (Object) ChatColor.GREEN + " Sinu na ucet");
	}

	private static double getMoney(InventoryClickEvent e) {
		Player hrac = (Player) e.getWhoClicked();
		Inventory inv = hrac.getOpenInventory().getTopInventory();
		double price = 0.0;
		for (ItemStack item : inv) {
			ItemUtils utils = new ItemUtils(item);
			if (!utils.isValid())
				continue;
			price += utils.getPrice() * item.getAmount();
		}
		Bukkit.getConsoleSender().sendMessage("Items price: " + String.valueOf(price));
		return price;
	}

}
