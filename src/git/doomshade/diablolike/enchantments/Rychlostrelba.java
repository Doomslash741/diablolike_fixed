/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.rit.sucy.CustomEnchantment
 *  org.bukkit.Material
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Projectile
 *  org.bukkit.event.entity.ProjectileLaunchEvent
 *  org.bukkit.util.Vector
 */
package git.doomshade.diablolike.enchantments;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import com.rit.sucy.CustomEnchantment;

public class Rychlostrelba extends CustomEnchantment {
	public Rychlostrelba() {
		super("Rychlostrelba", "rych");
		this.setMaxLevel(6);
		this.setBase(1.1);
		this.setInterval(0.1);
		this.setNaturalMaterials(new Material[] { Material.BOW });
		this.setTableEnabled(false);
	}

	@Override
	public void applyProjectileEffect(LivingEntity entity, int level, ProjectileLaunchEvent event) {
		event.getEntity().setVelocity(
				event.getEntity().getVelocity().multiply(this.getBase() + this.getInterval() * (double) (level - 1)));
	}
}
